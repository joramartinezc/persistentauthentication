package com.contauth.android.authenticatorremote;

import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;

public class MainActivity extends AppCompatActivity {

    private TextView finalResult;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        finalResult = (TextView) findViewById(R.id.result);

        final Button authBtn = (Button) findViewById(R.id.auth_btn);
        final Button shutBtn = (Button) findViewById(R.id.shut_btn);
        final EditText addressTxt = (EditText) findViewById(R.id.address);
        final TextView resultTxt = (TextView) findViewById(R.id.result);

        authBtn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                AsyncTaskRunner runner = new AsyncTaskRunner();

                String[] hostAdd = addressTxt.getText().toString().split(":");
                String address = hostAdd[0];
                String port = hostAdd[1];
                runner.execute(address, port, "authenticate");
            }
        });

        shutBtn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                AsyncTaskRunner runner = new AsyncTaskRunner();

                String[] hostAdd = addressTxt.getText().toString().split(":");
                String address = hostAdd[0];
                String port = hostAdd[1];
                runner.execute(address, port, "shutdown");
            }
        });
    }


    private class AsyncTaskRunner extends AsyncTask<String, String, String> {

        private String resp;
        ProgressDialog progressDialog;

        @Override
        protected String doInBackground(String... params) {
            publishProgress("Connecting..."); // Calls onProgressUpdate()
            String address = params[0];
            int port = Integer.parseInt(params[1]);
            try (Socket socket = new Socket(address, port);) {
                socket.setSoTimeout(5000);
                String message = params[2];
                ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream(1024);
                byte[] buffer = new byte[1024];

                int bytesRead;
                InputStream inputStream = socket.getInputStream();

                OutputStream outputStream = socket.getOutputStream();
                outputStream.write(message.getBytes());

                /*
                 * notice: inputStream.read() will block if no data return
                 */
                while ((bytesRead = inputStream.read(buffer)) != -1) {
                    byteArrayOutputStream.write(buffer, 0, bytesRead);
                    resp += byteArrayOutputStream.toString("UTF-8");
                }
            } catch (Exception e) {
                e.printStackTrace();
                resp = e.getMessage();
            }
            return resp;
        }

        @Override
        protected void onPostExecute(String result) {
            // execution of result of Long time consuming operation
            progressDialog.dismiss();
            finalResult.setText(result);
        }

        @Override
        protected void onPreExecute() {
            progressDialog = ProgressDialog.show(MainActivity.this, "ProgressDialog", "Authenticating...");
        }

        @Override
        protected void onProgressUpdate(String... text) {
            finalResult.setText(text[0]);
        }
    }

}
