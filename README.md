# README #
This is the system implementation for the thesis "Persistent Authentication in Smart Environments using COTS Hardware" for the universities DTU and KTH.

The main project is found under the folder PersistentAuth, the Initial authenticator is contained in the project FacialRecognition in the file authenticator.cpp, The project Authenticator remote works as a trigger button for starting the initial authenticator. The project homography can be used to generate the homography for each camera and their scene.