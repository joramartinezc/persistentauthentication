/*
 * authenticator.cpp
 *
 *  Created on: Apr 21, 2017
 *      Author: jorge
 */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>//socket
#include <sys/socket.h>//socket
#include <netinet/in.h>//socket
#include <arpa/inet.h>//socket
#include <iomanip>  // for controlling float print precision
#include <sstream>  // string to number conversion
#include <stdio.h>  // string to number conversion

#include <sys/un.h>

#include <iostream>
#include <vector>
#include <string>
#include <map>
#include <fcntl.h>

#include <opencv2/core.hpp>
#include <opencv2/highgui.hpp>
#include <opencv2/imgproc.hpp>
#include <opencv2/imgproc/imgproc_c.h>
#include <opencv2/face.hpp>
#include <opencv2/objdetect.hpp>

#include <gphoto2/gphoto2-camera.h>

const int im_width = 200;
const int im_height = 200;

template<typename T>
std::string N2S(T Number, int fill) {
	std::ostringstream ss;
	ss << std::setfill('0') << std::setw(fill) << std::setiosflags(std::ios_base::right) << Number;
	return ss.str();
}

void error(const char *msg) {
	perror(msg);
	exit(1);
}

int capture(const std::string filename, Camera *camera, GPContext *context) {
	int fd, retval;
	CameraFile *file;
	CameraFilePath camera_file_path;

	// take a shot
	retval = gp_camera_capture(camera, GP_CAPTURE_IMAGE, &camera_file_path, context);
	if (retval) {
		// do some error handling, probably return from this function
	}

	printf("Pathname on the camera: %s/%s\n", camera_file_path.folder, camera_file_path.name);

	fd = open(filename.c_str(), O_CREAT | O_WRONLY, 0644);
	// create new CameraFile object from a file descriptor
	retval = gp_file_new_from_fd(&file, fd);
	if (retval) {
	}

	// copy picture from camera
	retval = gp_camera_file_get(camera, camera_file_path.folder, camera_file_path.name, GP_FILE_TYPE_NORMAL, file, context);
	if (retval) {
	}

	printf("Deleting\n");
	// remove picture from camera memory
	retval = gp_camera_file_delete(camera, camera_file_path.folder, camera_file_path.name, context);
	if (retval) {
	}

	// free CameraFile object
	gp_file_free(file);

	int waittime = 2000;
	CameraEventType type;
	void *data;

	printf("Wait for events from camera\n");
	while (1) {
		retval = gp_camera_wait_for_event(camera, waittime, &type, &data, context);
		if (type == GP_EVENT_TIMEOUT) {
			break;
		} else if (type == GP_EVENT_CAPTURE_COMPLETE) {
			printf("Capture completed\n");
			waittime = 100;
		} else if (type != GP_EVENT_UNKNOWN) {
			printf("Unexpected event received from camera: %d\n", (int) type);
		}
	}
	return 0;
}

std::string recognizeFace(cv::Mat& frame) {
	cv::Ptr<cv::face::BasicFaceRecognizer> model = cv::face::createEigenFaceRecognizer();
	model->load("./out/eigenfaces_att.yml");
	model->setLabelInfo(1, "Jorge");
	model->setLabelInfo(2, "Ismael");
	model->setLabelInfo(3, "Agnieszka");
	model->setLabelInfo(0, "Luigi");
//	model->save("./out/eigenfaces_att.yml");
	// We are going to use the haar cascade you have specified in the
	// command line arguments:
	//
	cv::CascadeClassifier haar_cascade;
	haar_cascade.load("src/haarcascade_frontalface_default.xml");
	cv::Mat gray;
	cv::cvtColor(frame, gray, CV_BGR2GRAY);
	// Find the faces in the frame:
	std::vector<cv::Rect> faces;
	haar_cascade.detectMultiScale(gray, faces);
	int predictedLabel;
	std::string name = "error";
	for (auto& face_i : faces) {
		cv::Mat face = gray(face_i);
		// Since I am showing the Fisherfaces algorithm here, I also show how to resize the
		// face you have just found:
		cv::Mat face_resized;
		cv::resize(face, face_resized, cv::Size(im_width, im_height), 1.0, 1.0, cv::INTER_CUBIC);
		// The following line predicts the label of a given
		// test image:
		predictedLabel = model->predict(face_resized);
		name = model->getLabelInfo(predictedLabel);
		std::cout << "predicted: " << predictedLabel << " : " << name << std::endl << std::flush;
		cv::rectangle(frame, face_i, cv::Scalar(0, 255, 0));
		cv::putText(frame, name, face_i.br(), cv::FONT_HERSHEY_PLAIN, 1.0, cv::Scalar(0, 255, 0));
	}

	return name;
}

std::string authenticate() {

	Camera *camera;
	GPContext *context;
	gp_camera_new(&camera);
	context = gp_context_new();

	const cv::String& WIN_RF = "capture";
	cv::namedWindow(WIN_RF, cv::WINDOW_FULLSCREEN);
	cv::waitKey(500);

	int ret = gp_camera_init(camera, context);
	if (ret < GP_OK) {
		printf("No camera auto detected.\n");
		gp_camera_free(camera);
		return "err: no camera";
	}

	std::string filepath = "img_.jpg";
	capture(filepath, camera, context);
	cv::Mat frame = cv::imread(filepath);

	cv::resize(frame, frame, cv::Size(frame.cols / 5, frame.rows / 5), 1.0, 1.0, cv::INTER_CUBIC);
	std::string pId = recognizeFace(frame);

	cv::imshow(WIN_RF, frame);
	cv::waitKey(500);

	// close camera
	gp_camera_unref(camera);
	gp_context_unref(context);

	return pId;
}

char *socket_path = "../ContinuousAuth/socket";
void sendAuth(std::string userId) {
	struct sockaddr_un addr;
	char buf[100];
	int fd, rc;

	if ((fd = socket(AF_UNIX, SOCK_STREAM, 0)) == -1) {
		perror("socket error");
		exit(-1);
	}

	memset(&addr, 0, sizeof(addr));
	addr.sun_family = AF_UNIX;
	if (*socket_path == '\0') {
		*addr.sun_path = '\0';
		strncpy(addr.sun_path + 1, socket_path + 1, sizeof(addr.sun_path) - 2);
	} else {
		strncpy(addr.sun_path, socket_path, sizeof(addr.sun_path) - 1);
	}

	if (connect(fd, (struct sockaddr*) &addr, sizeof(addr)) == -1) {
		perror("connect error");
		exit(-1);
	}

	strcpy(buf,userId.c_str());
	rc = userId.size();
	if (write(fd, userId.c_str(), rc) != rc) {
		if (rc > 0)
			fprintf(stderr, "partial write");
		else {
			perror("write error");
			exit(-1);
		}
	}
}

char* QUIT = "shutdown";
char* AUTH = "authenticate";
//
int main(int argc, char **argv) {
//int main2(int argc, char **argv) {
	int sockfd, newsockfd;
	socklen_t clilen;
	char buffer[15];
	struct sockaddr_in serv_addr, cli_addr;
	sockfd = socket(AF_INET, SOCK_STREAM, 0);

	if (sockfd < 0)
		error("ERROR opening socket");

	bzero((char *) &serv_addr, sizeof(serv_addr));
	serv_addr.sin_family = AF_INET;
	serv_addr.sin_addr.s_addr = INADDR_ANY;
	serv_addr.sin_port = htons(8585);
	if (bind(sockfd, (struct sockaddr *) &serv_addr, sizeof(serv_addr)) < 0)
		error("ERROR on binding");
	listen(sockfd, 5);
	clilen = sizeof(cli_addr);

	while (strncmp(buffer, QUIT, 4) != 0) {
		newsockfd = accept(sockfd, (struct sockaddr *) &cli_addr, &clilen);
		if (newsockfd < 0)
			error("ERROR on accept");

	    struct timeval timeout;
	    timeout.tv_sec = 5;
	    timeout.tv_usec = 0;

	    if (setsockopt (newsockfd, SOL_SOCKET, SO_RCVTIMEO, (char *)&timeout,
	                sizeof(timeout)) < 0)
	        error("setsockopt SO_RCVTIMEO failed\n");

	    if (setsockopt (newsockfd, SOL_SOCKET, SO_SNDTIMEO, (char *)&timeout,
	                sizeof(timeout)) < 0)
	        error("setsockopt SO_SNDTIMEO failed\n");

		printf("server: got connection from %s port %d\n", inet_ntoa(cli_addr.sin_addr), ntohs(cli_addr.sin_port));
		send(newsockfd, "V1.0\n", 5, 0);
		bzero(buffer, 15);
		int n = read(newsockfd, buffer, 15);
		if (n < 0)
			error("ERROR reading from socket");

		printf("Here is the message: %s\n", buffer);
		std::cout << strncmp(buffer, AUTH, 12) << std::endl << std::flush;
		if (strncmp(buffer, "authenticate", 12) == 0) {
			std::string user = authenticate();
			std::string resp = "user: "+user+"\n";
			std::cout << resp << std::endl << std::flush;
			sendAuth(user);
			send(newsockfd, resp.c_str(), 5, 0);
		}
		std::cout << "auth finished" << std::endl << std::flush;

		close(newsockfd);
	}

	close(sockfd);
	return 0;
}
