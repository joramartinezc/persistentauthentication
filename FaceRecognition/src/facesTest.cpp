/*
 * facesTest.cpp
 *
 *  Created on: Apr 19, 2017
 *      Author: jorge
 */

#include <iostream>
#include <vector>
#include <string>
#include <map>
#include <fcntl.h>

#include <opencv2/core.hpp>
#include <opencv2/highgui.hpp>
#include <opencv2/imgproc.hpp>
#include <opencv2/imgproc/imgproc_c.h>
#include <opencv2/face.hpp>
#include <opencv2/objdetect.hpp>

#include <gphoto2/gphoto2-camera.h>

const int im_width = 200;
const int im_height = 200;

int capturePhoto (const std::string filename, Camera *camera, GPContext *context) {
 int fd, retval;
 CameraFile *file;
 CameraFilePath camera_file_path;

 // take a shot
 retval = gp_camera_capture(camera, GP_CAPTURE_IMAGE, &camera_file_path, context);
 if (retval) {
  // do some error handling, probably return from this function
 }

 printf("Pathname on the camera: %s/%s\n", camera_file_path.folder, camera_file_path.name);

 fd = open(filename.c_str(), O_CREAT | O_WRONLY, 0644);
 // create new CameraFile object from a file descriptor
 retval = gp_file_new_from_fd(&file, fd);
 if (retval) {}

 // copy picture from camera
 retval = gp_camera_file_get(camera, camera_file_path.folder, camera_file_path.name, GP_FILE_TYPE_NORMAL, file, context);
 if (retval) {}

 printf("Deleting\n");
 // remove picture from camera memory
 retval = gp_camera_file_delete(camera, camera_file_path.folder, camera_file_path.name,context);
 if (retval) {}

 // free CameraFile object
 gp_file_free(file);

 int waittime = 2000;
 CameraEventType type;
 void *data;

 printf("Wait for events from camera\n");
 while(1) {
  retval = gp_camera_wait_for_event(camera, waittime, &type, &data, context);
  if(type == GP_EVENT_TIMEOUT) {
   break;
  }
  else if (type == GP_EVENT_CAPTURE_COMPLETE) {
   printf("Capture completed\n");
   waittime = 100;
  }
  else if (type != GP_EVENT_UNKNOWN) {
   printf("Unexpected event received from camera: %d\n", (int)type);
  }
 }
 return 0;
}

void recognizeFaces(cv::Mat& frame) {
	cv::Ptr<cv::face::BasicFaceRecognizer> model = cv::face::createEigenFaceRecognizer();
	model->load("./out/eigenfaces_att.yml");
	model->setLabelInfo(1, "Jorge");
	model->setLabelInfo(2, "Ismael");
	model->setLabelInfo(3, "Agnieszka");
	model->setLabelInfo(0, "Luigi");
	model->save("./out/eigenfaces_att.yml");
	// We are going to use the haar cascade you have specified in the
	// command line arguments:
	//
	cv::CascadeClassifier haar_cascade;
	haar_cascade.load("src/haarcascade_frontalface_default.xml");
	cv::Mat gray;
	cv::cvtColor(frame, gray, CV_BGR2GRAY);
	// Find the faces in the frame:
	std::vector<cv::Rect> faces;
	haar_cascade.detectMultiScale(gray, faces);
	for (auto& face_i : faces) {
		cv::Mat face = gray(face_i);
		// Since I am showing the Fisherfaces algorithm here, I also show how to resize the
		// face you have just found:
		cv::Mat face_resized;
		cv::resize(face, face_resized, cv::Size(im_width, im_height), 1.0, 1.0, cv::INTER_CUBIC);
		// The following line predicts the label of a given
		// test image:
		int predictedLabel = model->predict(face_resized);
		std::string name = model->getLabelInfo(predictedLabel);
		std::cout << "predicted: " << predictedLabel << " : " << name << std::endl << std::flush;
		cv::rectangle(frame, face_i, cv::Scalar(0, 255, 0));
		cv::putText(frame, name, face_i.br(), cv::FONT_HERSHEY_PLAIN, 1.0, cv::Scalar(0, 255, 0));
	}
}

int main6(int argc, char **argv) {

	Camera *camera;
	GPContext *context;
	gp_camera_new (&camera);
	context = gp_context_new();


	const cv::String& WIN_RF = "capture";
	cv::namedWindow(WIN_RF, cv::WINDOW_NORMAL);
	cv::waitKey(10000);

	int ret = gp_camera_init(camera, context);
	if (ret < GP_OK) {
		printf("No camera auto detected.\n");
		gp_camera_free(camera);
		return 1;
	}

	std::string filepath = "img_.jpg";
	capturePhoto(filepath, camera, context);
	cv::Mat frame = cv::imread(filepath);

	cv::resize(frame, frame, cv::Size(frame.cols/5, frame.rows/5), 1.0, 1.0, cv::INTER_CUBIC);
	recognizeFaces(frame);

	cv::imshow(WIN_RF, frame);
	cv::waitKey(0);

	 // close camera
	 gp_camera_unref(camera);
	 gp_context_unref(context);

	return 0;
}

int main5(int argc, char **argv) {

//	cv::VideoCapture video;
//	video.open(0);
	cv::Mat frame;
//	video >> frame;

	cv::resize(frame, frame, cv::Size(frame.cols/10, frame.rows/10), 1.0, 1.0, cv::INTER_CUBIC);
	recognizeFaces(frame);

	cv::imshow("capture", frame);
	cv::waitKey(0);
//	video.release();
	return 0;
}



int main4(int argc, char **argv) {
	const cv::String& WIN_RF = "VideoPlayer";
	cv::namedWindow(WIN_RF,  cv::WINDOW_GUI_EXPANDED);
	cv::waitKey();
	cv::Mat frame = cv::Mat::zeros(cv::Size(2000, 1100), CV_8UC3);

	frame = cv::imread("img_.jpg");
//	cv::resize(frame, frame, cv::Size(frame.cols/10, frame.rows/10), 1.0, 1.0, cv::INTER_CUBIC);

	imshow(WIN_RF, frame);
	cv::waitKey();
	return 0;
}
