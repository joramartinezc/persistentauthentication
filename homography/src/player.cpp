#include <iostream> // for standard I/O
#include <string>   // for strings
#include <iomanip>  // for controlling float print precision
#include <sstream>  // string to number conversion
#include <stdio.h>  // string to number conversion
#include <algorithm>    // std::min

#include <opencv2/core.hpp>     // Basic OpenCV structures (cv::Mat, Scalar)
#include <opencv2/imgproc.hpp>  // initUndistortRectifyMap, remap, putText
#include <opencv2/videoio.hpp>
#include <opencv2/highgui.hpp>  // OpenCV window I/O
#include <opencv2/calib3d.hpp>

using namespace std;
using namespace cv;

char N_UP = -105;
char N_DOWN = -103;
char N_LEFT = -106;
char N_RIGHT = -104;

char UP = 'R';
char DOWN = 'T';
char LEFT = 'Q';
char RIGHT = 'S';
char ENTER = 13;

float ZERO = 0.0e1f;

template <typename T>
  string N2S ( T Number, int fill )
  {
     ostringstream ss;
     ss << std::setfill ('0') << setw(fill) << setiosflags(std::ios_base::right) << Number;
     return ss.str();
  }

void loadParameters(const string& filename, Size imgSize, OutputArray map1, OutputArray map2, bool useFisheye = true) {
	FileStorage fs;
	fs.open( filename, FileStorage::READ );

	if( ! fs.isOpened() ){
		cerr << "Failed to open: " << filename << endl << flush;
	}

	Mat cameraMatrix;
	fs["camera_matrix"] >> cameraMatrix;
	Mat distCoeffs;
	fs["distortion_coefficients"] >> distCoeffs;
	String time;
	fs["calibration_time"] >> time;
	cout << "Calibration Time: " << time << endl << flush;
	Mat empty;
//	Mat newMat = g,etDefaultNewCameraMatrix(cameraMatrix);
	float f = 200;
	float data[10] = {f,0,800,
					  0,f,550,
					  0,0,1  };
	Mat newMat(3,3,CV_32F,data);
//	cout << newMat<<endl<<flush;
//	initUndistortRectifyMap(cameraMatrix, distCoeffs, empty, newMat, imgSize, CV_16SC2, map1, map2);
	if (useFisheye) {
//		Mat newMat;
//		fisheye::estimateNewCameraMatrixForUndistortRectify(cameraMatrix, distCoeffs, imgSize, Matx33d::eye(), newMat, 1);
		fisheye::initUndistortRectifyMap(cameraMatrix, distCoeffs, Matx33d::eye(), newMat, imgSize, CV_16SC2, map1, map2);
	} else {
//		newMat = getOptimalNewCameraMatrix(cameraMatrix, distCoeffs, imgSize, 1, imgSize, 0);
		initUndistortRectifyMap(cameraMatrix, distCoeffs, empty, newMat, imgSize, CV_16SC2, map1, map2);
	}

    fs.release();
}

int main(int argc, char *argv[]) {
//	system("paplay /usr/share/sounds/freedesktop/stereo/complete.oga");

//	const string sourcePath = argv[1];

	const string camId = "205";

	const string sourcePath = "rtsp://192.168.1."+camId+":554/v00";
	stringstream conv;
	VideoCapture video;
	if ((sourcePath.find_first_not_of("0123456789") == string::npos)) {
		conv << sourcePath;
		int camId;
		conv >> camId;
		video.open(camId);
	} else {
		video = VideoCapture(sourcePath);
	}
	if (!video.isOpened()) {
		cout << "Could not open reference " << sourcePath << endl;
		return -1;
	}

//
	Mat map1, map2;
	Size imgSize(video.get(CV_CAP_PROP_FRAME_WIDTH), video.get(CV_CAP_PROP_FRAME_HEIGHT));
	loadParameters("src/FCS-3091_domecam_c.xml", imgSize, map1, map2);

	const String& WIN_RF = "VideoPlayer";
	namedWindow(WIN_RF, WINDOW_NORMAL);

	// Read real word points
	vector<Point2f> obj;
	std::vector<Point2f> scene;
	FileStorage fs;
	fs.open( "src/corners"+camId+".xml", FileStorage::READ );
	double x,y;
	Point2f point;

	fs["corner1_x"] >> x;
	fs["corner1_y"] >> y;
	point = Point2f(x,y);
	obj.push_back( point );

	fs["corner2_x"] >> x;
	fs["corner2_y"] >> y;
	point = Point2f(x,y);
	obj.push_back( point );

	fs["corner3_x"] >> x;
	fs["corner3_y"] >> y;
	point = Point2f(x,y);
	obj.push_back( point );

	fs["corner4_x"] >> x;
	fs["corner4_y"] >> y;
	point = Point2f(x,y);
	obj.push_back( point );
	fs.release();
	//END Read realworld points

	int k=0;
	cout << "Select point "<<k+1<<": "<<obj.at(k) << endl << flush;

	Mat frame;
	Point2f cursor(0,0);
	for (int i = 0;;i++) {
		video >> frame;
		if (frame.empty()) {
			cout << " < < <  Video over!  > > > ";
			break;
		}

		remap(frame, frame, map1, map2, INTER_LINEAR);
		Point2f prntCursor(cursor.x-13,cursor.y+9);
		putText(frame, "+"+N2S(k+1,0), prntCursor, CV_FONT_HERSHEY_SIMPLEX, 1, cv::Scalar(0, 255, 255));

		imshow(WIN_RF, frame);
		char c = (char) waitKey(1);
		if (c == 27){
			break;
		} else if (c == UP || c == N_UP){
			float inc = c == N_UP ? 10.0f : 1.0f;
			float x = cursor.x;
			float y = std::max(ZERO,(cursor.y-inc));
			cursor = Point2f(x,y);
			cout << "cursor: "<<cursor << endl << flush;
		} else if (c == DOWN || c == N_DOWN){
			float inc = c == N_DOWN ? 10.0f : 1.0f;
			float x = cursor.x;
			float y = std::min((float)(video.get(CV_CAP_PROP_FRAME_HEIGHT)-1),(cursor.y+inc));
			cursor = Point2f(x,y);
			cout << "cursor: "<<cursor << endl << flush;
		} else if (c == LEFT || c == N_LEFT){
			float inc = c == N_LEFT ? 10.0f : 1.0f;
			float x = std::max(ZERO,(cursor.x-inc));
			float y = cursor.y;
			cursor = Point2f(x,y);
			cout << "cursor: "<<cursor << endl << flush;
		} else if (c == RIGHT || c == N_RIGHT){
			float inc = c == N_RIGHT ? 10.0f : 1.0f;
			float x = std::min((float)(video.get(CV_CAP_PROP_FRAME_WIDTH)-1),(cursor.x+inc));
			float y = cursor.y;
			cursor = Point2f(x,y);
			cout << "cursor: "<<cursor << endl << flush;
		} else if (c == ENTER){
			k++;
			scene.push_back( cursor );
			cv::Mat roi = frame(cv::Range(cursor.y-10, cursor.y+10), cv::Range(cursor.x-10, cursor.x+10));
			cv::Mat1b mask(roi.rows, roi.cols);
			cv::Scalar mean = cv::mean(roi, mask);
			cout << "Color: "<<mean<<endl<<flush;
			if(scene.size() >= 4) {
				break;
			} else {
				cout << "Select point "<<k+1<<": "<<obj.at(k) << endl << flush;
			}
		} else if(c != -1){
			cout << "unrecognized input: "<< (int)c << endl << flush;
		}
	}

	//estimate homography
	Mat H = findHomography( obj, scene, CV_RANSAC );
	cout << H;
	fs = FileStorage( "src/homography"+camId+".xml", FileStorage::WRITE );
	fs << "homography" << H;
	fs.release();

}
