////////////////////////////////////////////////////////////////////////////////
// Copyright 2017 Jorge Andres Martinez Castillo
// 
// Licensed under the Apache License, Version 2.0 (the "License"); you may not
// use this file except in compliance with the License.  You may obtain a copy
// of the License at
// 
//   http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
// WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
// License for the specific language governing permissions and limitations under
// the License.
////////////////////////////////////////////////////////////////////////////////
/*
 * histTest.cpp
 *
 *  Created on: May 25, 2017
 *      Author: jorge
 */

#include <core/mat.hpp>
#include <core/mat.inl.hpp>
#include <core/types.hpp>
#include <highgui.hpp>
#include <imgcodecs/imgcodecs_c.h>
#include <imgcodecs.hpp>
#include <iostream>
#include <memory>

#include "../src/entities/Frame.h"
#include "../src/identity/CredResult.h"
#include "../src/identity/HistogramId.h"
#include "../src/utils/DebugWin.h"

using namespace cv;

int main2(int argc, char **argv) {
//int main(int argc, char **argv) {

	Mat jImg = imread("jorge1.png");
	Mat jMask = imread("jmask.png",CV_LOAD_IMAGE_GRAYSCALE);

	HistogramId Hid = HistogramId();
	RotatedRect box = cv::RotatedRect(Point2f(jImg.cols/2,jImg.rows/2),jImg.size(),0.);
	std::shared_ptr<Mat> ptrFrame = std::make_shared<Mat>(jImg);
	Frame jFrame = Frame(ptrFrame,0);
	Hid.init(jFrame, jMask, box, Point2f(0,0));

	Mat aImg = imread("aga1.png");
	Mat aMask = imread("amask.png",CV_LOAD_IMAGE_GRAYSCALE);

	box = cv::RotatedRect(Point2f(aImg.cols/2,aImg.rows/2),aImg.size(),0.);
	std::shared_ptr<Mat> ptrAFrame = std::make_shared<Mat>(aImg);
	Frame aFrame = Frame(ptrAFrame,0);
	std::shared_ptr<CredResult> res = Hid.checkSimilarity(aFrame,aMask,box,Point2f(0,0));

	std::cout << res->getResult() << std::flush;

	DebugWin::getInstance().printDebug();
	waitKey();
}
