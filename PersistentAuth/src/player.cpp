////////////////////////////////////////////////////////////////////////////////
// Copyright 2017 Jorge Andres Martinez Castillo
// 
// Licensed under the Apache License, Version 2.0 (the "License"); you may not
// use this file except in compliance with the License.  You may obtain a copy
// of the License at
// 
//   http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
// WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
// License for the specific language governing permissions and limitations under
// the License.
////////////////////////////////////////////////////////////////////////////////
#include <iostream> // for standard I/O
#include <string>   // for strings
#include <iomanip>  // for controlling float print precision
#include <sstream>  // string to number conversion
#include <list>
#include <stdio.h>
#include <algorithm>
#include <chrono>
#include <thread> // sleep

#include <opencv2/core.hpp>     // Basic OpenCV structures (cv::Mat, Scalar)
#include <opencv2/videoio.hpp>
#include <opencv2/highgui.hpp>  // OpenCV window I/O
#include <opencv2/imgcodecs.hpp>
#include <opencv2/imgproc.hpp>
#include <opencv2/video.hpp>
#include <opencv2/video/background_segm.hpp>
#include <opencv2/photo.hpp>// Img noise

#include <opencv2/objdetect.hpp>

#include "identity/Blob.h"
#include "identity/Credential.h"
#include "identity/HistogramId.h"
#include "identity/IdentityAnalyser.h"
#include "identity/Principal.h"

#include "flow/BgnSubtraction.h"
#include "flow/CameraTransformation.h"
#include "flow/Denoise.h"
#include "flow/Dilate.h"
#include "flow/Transformation.h"
#include "flow/Treshold.h"

#include "sceneModel/Environment.h"
#include "sceneModel/environmentSettings/CamSettings.h"
#include "sceneModel/environmentSettings/ZoneSettings.h"

#include "gui/Authenticator.h"

#include "utils/Utils.h"
#include "utils/DebugWin.h"

using namespace std;
using namespace cv;

std::list<Blob*> histVect;

void print2(const String& winName, const std::vector<std::shared_ptr<cv::Mat>> frames) {
	if (frames.size() > 0) {
		//	const String& winName = "win";
		int width, height;
		width = frames[0]->size().width;
		height = frames[0]->size().height;
		Mat frameAll;
//		cout << "frame0: "<<frames[0]->channels()<< " frameAll:"<<frameAll.channels()<<endl<<flush;
		frameAll = Mat(height + 10, (frames.size() < 3 ? frames.size() : 3) * width + 15, CV_MAKETYPE(8, 3), CV_RGB(100, 100, 100));
		frames[0]->copyTo(Mat(frameAll, Rect(5, 5, width, height)));
		for (unsigned i = 1; i < frames.size() && i < 3; ++i) {
			frames[i]->copyTo(Mat(frameAll, Rect(5 + width * i, 5, width, height)));
		}
		imshow(winName, frameAll);
	}
}

void printZone(int idCam, int idZone, std::shared_ptr<cv::Mat> drawing) {
	std::shared_ptr<Zone> zone = Environment::getInstance().getZone(idZone);
	std::shared_ptr<Camera> cam = Environment::getInstance().getCamera(idCam);

	const cv::Mat H = cam->getH();
	std::vector<cv::Point> contCopy = zone->getContour();

	for (unsigned i = 0; i < contCopy.size(); i++) {

		cv::Point pnt = contCopy[i];
		cv::Mat_<double> pntMat(3, 1);
		double x = ((double) pnt.x), y = ((double) pnt.y), z;
		pntMat(0, 0) = x;
		pntMat(1, 0) = y;
		pntMat(2, 0) = 1.0;

		cv::Mat_<double> dst = H * pntMat;
		x = dst(0, 0);
		y = dst(1, 0);
		z = dst(2, 0);
		contCopy[i] = Point(x / z, y / z);
	}

	std::vector<std::vector<cv::Point> > contours(1);
	contours[0] = contCopy;

	cv::drawContours(*drawing.get(), contours, 0, cv::Scalar(0, 255, 0));
}

//int main1(int argc, char *argv[]) {
int main(int argc, char *argv[]) {
	cout << "OpenCV Version used:" << CV_MAJOR_VERSION << "." << CV_MINOR_VERSION << endl;

	Environment& env = Environment::getInstance();
	CamSettings cSet205 = CamSettings();

	float baseDelay = 000;
	std::string baseFolder = "/home/jorge/Videos/EXP";

	std::string video = "1.4.1";

	cSet205.idCamera = 205;
//	cSet205.sourcePath = "rtsp://192.168.1.205:554/v00";
	cSet205.sourcePath = baseFolder+"/stream205_"+video+".mp4";
	cSet205.settingsFilePath = "settings/FCS-3091_domecam_c.xml";
	cSet205.homographyFilePath = "settings/homography205.xml";
	cSet205.orientation = Camera::TOP;
	cSet205.isFisheye = true;
	cSet205.focalLenght = 200;
	cSet205.delay = 00+baseDelay;

	CamSettings cSet206 = CamSettings();
	cSet206.idCamera = 206;
//	cSet206.sourcePath = "rtsp://192.168.1.206:554/v00";
	cSet206.sourcePath = baseFolder+"/stream206_"+video+".mp4";
	cSet206.settingsFilePath = "settings/FCS-3091_domecam_c.xml";
	cSet206.homographyFilePath = "settings/homography206.xml";
	cSet206.orientation = Camera::TOP;
	cSet206.isFisheye = true;
	cSet206.focalLenght = 200;
	cSet206.delay = 00+baseDelay;


	env.registerCameras(vector<CamSettings> { cSet205, cSet206 });

	ZoneSettings zSet = ZoneSettings();
	zSet.idZone = 1;
	zSet.name = "Entrance";
	zSet.contoursFilePath = "settings/zoneDoor.xml";
	env.registerAuthenticationZone(vector<ZoneSettings> { zSet });

	env.associateCameraToAuthenticationZone(206, 1);

	Authenticator auth;
	auth.listenAuth();

	int delay = 1;
	const String& WIN_RF = "VideoPlayer";
	namedWindow(WIN_RF, WINDOW_NORMAL);
	while (true) {
		std::map<int, std::shared_ptr<cv::Mat>> results = env.retrieveResults();

		std::vector<std::shared_ptr<cv::Mat>> vframes;
		std::shared_ptr<cv::Mat> frame;
		for (auto& kv : results) {
			frame = kv.second;
			printZone(kv.first, zSet.idZone, frame);
			vframes.push_back(frame);
		}

		print2(WIN_RF, vframes);
		DebugWin::getInstance().printDebug();
//		cv::imshow(WIN_RF, *vframes[0]);

		char c = (char) waitKey(delay);
		if (c >= 0) cout << ">>>pressed key: "<<(int)c<<endl<<flush;
		if (c == 27) {
			env.stopCameras();
			auth.stop();
			break;
		} else if (c >= 48 && c <= 57) {
			std::shared_ptr<Principal> ppal = std::make_shared<Principal>();
			int zoneId = c - 48;

			if(zoneId == 1) {
				ppal->setPrincipalId("AAA");
			}
			if(zoneId == 2) {
				ppal->setPrincipalId("BBB");
				zoneId = 1;
			}
			if(zoneId == 2) {
				ppal->setPrincipalId("CCC");
				zoneId = 1;
			}
			if(zoneId == 3) {
				ppal->setPrincipalId("DDD");
				zoneId = 1;
			}
			if(zoneId == 4) {
				ppal->setPrincipalId("EEE");
				zoneId = 1;
			}
			if(zoneId == 5) {
				ppal->setPrincipalId("FFF");
				zoneId = 1;
			}

//			std::cout << "Authenticating Principal: " << jorge->getPrincipalId() << " in zone " << zoneId << endl << flush;
			Utils::log(vector<std::string>({"Authenticating Principal: ", ppal->getPrincipalId(), " in zone ", Utils::n2s(zoneId,1)}));
			env.notifyCamerasOfZone(zoneId, ppal);
		} else if(c == 113) {
			Utils::log(vector<string>({"Pausing video"}));
			for (auto& kv : env.cameras) {
				kv.second->fps = 0;
				waitKey(0);
			}
		} else if(c == 112) {
			Utils::log(vector<string>({"Slowing video"}));
			for (auto& kv : env.cameras) {
				kv.second->fps = 1;
			}
		}
	}

	return 0;
}
