////////////////////////////////////////////////////////////////////////////////
// Copyright 2017 Jorge Andres Martinez Castillo
// 
// Licensed under the Apache License, Version 2.0 (the "License"); you may not
// use this file except in compliance with the License.  You may obtain a copy
// of the License at
// 
//   http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
// WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
// License for the specific language governing permissions and limitations under
// the License.
////////////////////////////////////////////////////////////////////////////////
/*
 * Authenticator.cpp
 *
 *  Created on: Apr 24, 2017
 *      Author: jorge
 */

#include "Authenticator.h"

#include <string>
#include <iostream>
#include <stdio.h>
#include <unistd.h>
#include <sys/socket.h>
#include <sys/un.h>
#include <stdlib.h>

#include <memory>
#include <thread>

#include "../sceneModel/Environment.h"
#include "../identity/Principal.h"
#include "../utils/Utils.h"

const int zoneId = 1;

Authenticator::Authenticator() {
	socketFd = -1;

}

Authenticator::~Authenticator() {
	stop();
}
void Authenticator::init() {
	std::string path = "./socket";
	char *socket_path = (char*)path.c_str();
	struct sockaddr_un addr;
	char buf[100];
	int cl, rc;

	if ((socketFd = socket(AF_UNIX, SOCK_STREAM, 0)) == -1) {
		perror("socket error");
		exit(-1);
	}

	memset(&addr, 0, sizeof(addr));
	addr.sun_family = AF_UNIX;
	if (*socket_path == '\0') {
		*addr.sun_path = '\0';
		strncpy(addr.sun_path + 1, socket_path + 1, sizeof(addr.sun_path) - 2);
	} else {
		strncpy(addr.sun_path, socket_path, sizeof(addr.sun_path) - 1);
		unlink (socket_path);
	}

	if (bind(socketFd, (struct sockaddr*) &addr, sizeof(addr)) == -1) {
		perror("bind error");
		exit(-1);
	}

	if (listen(socketFd, 5) == -1) {
		perror("listen error");
		exit(-1);
	}
	Environment& env = Environment::getInstance();
	while (running) {
		if ((cl = accept(socketFd, NULL, NULL)) == -1) {
			if (socketFd != -1) {
				perror("accept error");
			}
			continue;
		}

	    struct timeval timeout;
	    timeout.tv_sec = 1;
	    timeout.tv_usec = 0;

	    if (setsockopt (cl, SOL_SOCKET, SO_RCVTIMEO, (char *)&timeout,
	                sizeof(timeout)) < 0)
	        perror("setsockopt SO_RCVTIMEO failed\n");

	    if (setsockopt (cl, SOL_SOCKET, SO_SNDTIMEO, (char *)&timeout,
	                sizeof(timeout)) < 0)
	        perror("setsockopt SO_SNDTIMEO failed\n");

		while ((rc = read(cl, buf, sizeof(buf))) > 0) {
			printf("read %u bytes: %.*s\n", rc, rc, buf);
			std::shared_ptr<Principal> jorge = std::make_shared<Principal>();
			std::string nameStr = std::string(buf,rc);
			jorge->setPrincipalId(nameStr);
			Utils::log(std::vector<std::string>({"Authenticating Principal: ", jorge->getPrincipalId(), " in zone ", Utils::n2s(zoneId,1)}));
			env.notifyCamerasOfZone(zoneId, jorge);
		}
		if (rc == -1) {
			perror("Error on read");
//			exit(-1);
		} else if (rc == 0) {
			printf("read EOF\n");
			close(cl);
		}
	}

}

void Authenticator::listenAuth() {
	running = true;
	authThread = std::thread(&Authenticator::init,this);
}

void Authenticator::stop() {
	running = false;
	if(socketFd != -1) {
		shutdown(socketFd,SHUT_RDWR);
		socketFd = -1;
	}
	if(authThread.joinable()) {
		authThread.join();
	}
}

int main12(int argc, char **argv) {
	Authenticator auth;
	auth.listenAuth();
	while(true){
		std::this_thread::sleep_for(std::chrono::milliseconds(1000));
	}
}

