/*******************************************************************************
 * Copyright 2017 Jorge Andres Martinez Castillo
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License.  You may obtain a copy
 * of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations under
 * the License.
 ******************************************************************************/
/*
 * Frame.h
 *
 *  Created on: May 3, 2017
 *      Author: jorge
 */

#ifndef ENTITIES_FRAME_H_
#define ENTITIES_FRAME_H_

#include <memory>
#include <opencv2/core.hpp>

class Frame {
public:
	Frame();
	Frame(std::shared_ptr<cv::Mat>& frameMat ,double msec);
	virtual ~Frame();
	const std::shared_ptr<cv::Mat>& getFrameMat() const;
	void setFrameMat(const std::shared_ptr<cv::Mat>& frameMat);
	double getMsec() const;
	void setMsec(double msec);

private:
	std::shared_ptr<cv::Mat> frameMat;
	double msec;
};

#endif /* ENTITIES_FRAME_H_ */
