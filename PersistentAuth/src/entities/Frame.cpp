////////////////////////////////////////////////////////////////////////////////
// Copyright 2017 Jorge Andres Martinez Castillo
// 
// Licensed under the Apache License, Version 2.0 (the "License"); you may not
// use this file except in compliance with the License.  You may obtain a copy
// of the License at
// 
//   http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
// WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
// License for the specific language governing permissions and limitations under
// the License.
////////////////////////////////////////////////////////////////////////////////
/*
 * Frame.cpp
 *
 *  Created on: May 3, 2017
 *      Author: jorge
 */

#include "Frame.h"

Frame::Frame() {
	msec = -1;
}

Frame::Frame(std::shared_ptr<cv::Mat>& matPtr, double msec) {
	this->frameMat = matPtr;
	this->msec = msec;
}

Frame::~Frame() {}

const std::shared_ptr<cv::Mat>& Frame::getFrameMat() const {
	return frameMat;
}

void Frame::setFrameMat(const std::shared_ptr<cv::Mat>& frameMat) {
	this->frameMat = frameMat;
}

double Frame::getMsec() const {
	return msec;
}

void Frame::setMsec(double msec) {
	this->msec = msec;
}
