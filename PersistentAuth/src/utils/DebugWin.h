/*******************************************************************************
 * Copyright 2017 Jorge Andres Martinez Castillo
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License.  You may obtain a copy
 * of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations under
 * the License.
 ******************************************************************************/
/*
 * DebugWin.h
 *
 *  Created on: May 22, 2017
 *      Author: jorge
 */

#ifndef UTILS_DEBUGWIN_H_
#define UTILS_DEBUGWIN_H_

#include <opencv2/core.hpp>

#include "LimitBuffer.h"
#include "DebugMat.h"

class DebugWin {
public:
	static DebugWin& getInstance();
	virtual ~DebugWin();
	void enqueDebug(const cv::String& winName, const cv::Mat& mat);
	void printDebug();
private:
	DebugWin();
	LimitBuffer<DebugMat>* buffer;
};

#endif /* UTILS_DEBUGWIN_H_ */
