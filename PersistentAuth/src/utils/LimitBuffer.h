/*******************************************************************************
 * Copyright 2017 Jorge Andres Martinez Castillo
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License.  You may obtain a copy
 * of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations under
 * the License.
 ******************************************************************************/
/*
 * LimitBuffer.h
 *
 *  Created on: Apr 17, 2017
 *      Author: jorge
 */

#ifndef UTILS_LIMITBUFFER_H_
#define UTILS_LIMITBUFFER_H_
#include <list>
#include <mutex>

template<class T>
class LimitBuffer {
public:
	LimitBuffer();
	LimitBuffer(const int& size);
	virtual ~LimitBuffer();

	void enqueue(T obj);
	T dequeue();
	int getMaxSize();
	int size();
private:
	std::list<T> q;
	unsigned maxSize;
	std::mutex frameMutex;
};

#endif /* UTILS_LIMITBUFFER_H_ */
