/*******************************************************************************
 * Copyright 2017 Jorge Andres Martinez Castillo
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License.  You may obtain a copy
 * of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations under
 * the License.
 ******************************************************************************/
/*
 * Utils.h
 *
 *  Created on: Apr 15, 2017
 *      Author: jorge
 */

#ifndef UTILS_UTILS_H_
#define UTILS_UTILS_H_
#include <string>
#include <vector>
#include <iostream>

#include <opencv2/core.hpp>     // Basic OpenCV structures (cv::Mat, Scalar)
using namespace cv;

class Utils {

public:
	Utils();
	virtual ~Utils();
	static void rotatedRectangle(Mat img, RotatedRect rect, const Scalar& color,
	        int thickness = 1, int lineType = LINE_8, int shift = 0, bool filled = false);
	static void log(std::vector<std::string> str);
	template <typename T>
	static std::string n2s(const T& number, int fill = 0);
	static std::string getDatetime();
	static void printMat(Mat mat, int prec = 1);
};

#endif /* UTILS_UTILS_H_ */
