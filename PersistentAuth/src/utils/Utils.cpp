////////////////////////////////////////////////////////////////////////////////
// Copyright 2017 Jorge Andres Martinez Castillo
// 
// Licensed under the Apache License, Version 2.0 (the "License"); you may not
// use this file except in compliance with the License.  You may obtain a copy
// of the License at
// 
//   http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
// WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
// License for the specific language governing permissions and limitations under
// the License.
////////////////////////////////////////////////////////////////////////////////
/*
 * Utils.cpp
 *
 *  Created on: Apr 15, 2017
 *      Author: jorge
 */

#include "Utils.h"
#include <thread>
#include <algorithm>    // std::max
#include <vector>
#include <string>
#include <iostream>
#include <iomanip>  // for controlling float print precision
#include <sstream>  // string to number conversion
#include <stdio.h>  // string to number conversion
#include <opencv2/imgproc.hpp>
#include <unistd.h>
#include <chrono>  // chrono::system_clock
#include <ctime>   // localtime
#include <sstream> // stringstream
#include <iomanip> // put_time

#include <execinfo.h>  // for backtrace
#include <dlfcn.h>     // for dladdr
#include <cxxabi.h>    // for __cxa_demangle

using namespace cv;
using namespace std;

Utils::Utils() {
	// TODO Auto-generated constructor stub

}

Utils::~Utils() {
	// TODO Auto-generated destructor stub
}

void Utils::rotatedRectangle(Mat img, RotatedRect rect, const Scalar& color, int thickness, int lineType, int shift, bool filled) {

	Point2f vertices2f[4];
	Point2i vertices[4];
	rect.points(vertices2f);
	for (int i = 0; i < 4; i++) {
		line(img, vertices2f[i], vertices2f[(i + 1) % 4], color, thickness, lineType, shift);
		vertices[i] = vertices2f[i];
	}
	if (filled) {
		cv::fillConvexPoly(img, vertices, 4, color, lineType, shift);
	}
}

//https://gist.github.com/fmela/591333/c64f4eb86037bb237862a8283df70cdfc25f01d3
std::string prettyBacktrace(int skip = 2, int skipEnd = 3) {
	void *callstack[128];
	const int nMaxFrames = sizeof(callstack) / sizeof(callstack[0]);
	char buf[1024];
	int nFrames = backtrace(callstack, nMaxFrames);
	char **symbols = backtrace_symbols(callstack, nFrames);
	nFrames = std::min(nFrames, skipEnd);

	std::ostringstream trace_buf;
	for (int i = skip; i < nFrames; i++) {
		Dl_info info;
		if (dladdr(callstack[i], &info)) {
			char *demangled = NULL;
			int status;
			demangled = abi::__cxa_demangle(info.dli_sname, NULL, 0, &status);
//			snprintf(buf, sizeof(buf), "%-3d %*0p %s + %zd\n",
//					 i, 2 + sizeof(void*) * 2, callstack[i],
//					 status == 0 ? demangled : info.dli_sname,
//					 (char *)callstack[i] - (char *)info.dli_saddr);
			string msg = status == 0 ? demangled : info.dli_sname;
			trace_buf << msg;
			free(demangled);
		} else {
			snprintf(buf, sizeof(buf), "%-3d %*0p\n", i, 2 + sizeof(void*) * 2, callstack[i]);
			trace_buf << buf;
		}
//
//		snprintf(buf, sizeof(buf), "%s\n", symbols[i]);
//		trace_buf << buf;
		if ((i + 1) < nFrames) {
			trace_buf << "\n";
		}
	}
	free(symbols);
	if (nFrames == nMaxFrames)
		trace_buf << "[truncated]\n";
	return trace_buf.str();
}

void Utils::log(vector<string> str_v) {
	cout << "[" << getDatetime() << "] [" << std::this_thread::get_id() << "] [" << prettyBacktrace() << "]: " << endl << "\t";
	for (auto& str : str_v) {
		cout << str;
	}
	cout << endl << endl << flush;
}

template<typename T>
string Utils::n2s(const T& number, int fill) {
	ostringstream ss;
	ss << std::setfill('0') << setw(fill) << setiosflags(std::ios_base::right) << number;
	return ss.str();
}

string Utils::getDatetime() {
	auto now = std::chrono::system_clock::now();
	auto in_time_t = std::chrono::system_clock::to_time_t(now);
	auto ms = std::chrono::duration_cast<std::chrono::milliseconds>(now.time_since_epoch()) - std::chrono::duration_cast<std::chrono::seconds>(now.time_since_epoch());

	std::stringstream ss;
	ss << std::put_time(std::localtime(&in_time_t), "%H h %M m %S s ");
	ss << ms.count() << " ms";
	return ss.str();
}

void Utils::printMat(Mat mat, int prec) {
	for (int i = 0; i < mat.size().height; i++) {
		cout << "[";
		for (int j = 0; j < mat.size().width; j++) {
			cout << setprecision(prec) << mat.at<double>(i, j);
			if (j != mat.size().width - 1)
				cout << ", ";
			else
				cout << "]" << endl;
		}
	}
	cout << endl;
}

template string Utils::n2s<int>(const int& number, int fill = 1);
template string Utils::n2s<double>(const double& number, int fill = 1);
template string Utils::n2s<float>(const float& number, int fill = 1);
template string Utils::n2s<unsigned>(const unsigned& number, int fill = 1);
template string Utils::n2s<long>(const long& number, int fill = 1);
template string Utils::n2s<unsigned long>(const unsigned long& number, int fill = 1);
