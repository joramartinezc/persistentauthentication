////////////////////////////////////////////////////////////////////////////////
// Copyright 2017 Jorge Andres Martinez Castillo
// 
// Licensed under the Apache License, Version 2.0 (the "License"); you may not
// use this file except in compliance with the License.  You may obtain a copy
// of the License at
// 
//   http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
// WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
// License for the specific language governing permissions and limitations under
// the License.
////////////////////////////////////////////////////////////////////////////////
/*
 * DebugWin.cpp
 *
 *  Created on: May 22, 2017
 *      Author: jorge
 */

#include <opencv2/highgui.hpp>

#include "DebugWin.h"

DebugWin::DebugWin() {
	this->buffer = new LimitBuffer<DebugMat>();
}
DebugWin::~DebugWin() {}

DebugWin& DebugWin::getInstance(){
  static DebugWin instance;
  return instance;
}

void DebugWin::enqueDebug(const cv::String& winName, const cv::Mat& mat) {
	this->buffer->enqueue(DebugMat(winName,mat));
}

void DebugWin::printDebug() {
	while(buffer->size() > 0) {
		DebugMat dMat = buffer->dequeue();
		cv::imshow(dMat.getWinName(),dMat.getMat());
	}
}
template class LimitBuffer<DebugMat>;
