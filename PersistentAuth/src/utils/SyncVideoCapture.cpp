////////////////////////////////////////////////////////////////////////////////
// Copyright 2017 Jorge Andres Martinez Castillo
// 
// Licensed under the Apache License, Version 2.0 (the "License"); you may not
// use this file except in compliance with the License.  You may obtain a copy
// of the License at
// 
//   http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
// WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
// License for the specific language governing permissions and limitations under
// the License.
////////////////////////////////////////////////////////////////////////////////
/*
 * SyncCameraRecorder.cpp
 *
 *  Created on: Apr 16, 2017
 *      Author: jorge
 */

#include "SyncVideoCapture.h"

std::mutex SyncVideoCapture::frameMutex;

bool SyncVideoCapture::grab() {
    std::lock_guard<std::mutex> lock(SyncVideoCapture::frameMutex);
    return cv::VideoCapture::grab();
}

bool SyncVideoCapture::retrieve(cv::OutputArray image, int flag) {
    std::lock_guard<std::mutex> lock(SyncVideoCapture::frameMutex);
    return cv::VideoCapture::retrieve(image,flag);
}
