/*******************************************************************************
 * Copyright 2017 Jorge Andres Martinez Castillo
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License.  You may obtain a copy
 * of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations under
 * the License.
 ******************************************************************************/
/*
 * DebugMat.h
 *
 *  Created on: May 22, 2017
 *      Author: jorge
 */

#ifndef UTILS_DEBUGMAT_H_
#define UTILS_DEBUGMAT_H_
#include <opencv2/core.hpp>

class DebugMat {
public:
	DebugMat(cv::String winName, cv::Mat mat);
	virtual ~DebugMat();
	const cv::Mat& getMat() const;
	const cv::String& getWinName() const;
private:
	cv::String winName;
	cv::Mat mat;
};

#endif /* UTILS_DEBUGMAT_H_ */
