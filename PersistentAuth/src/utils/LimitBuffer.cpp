////////////////////////////////////////////////////////////////////////////////
// Copyright 2017 Jorge Andres Martinez Castillo
// 
// Licensed under the Apache License, Version 2.0 (the "License"); you may not
// use this file except in compliance with the License.  You may obtain a copy
// of the License at
// 
//   http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
// WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
// License for the specific language governing permissions and limitations under
// the License.
////////////////////////////////////////////////////////////////////////////////
/*
 * LimitBuffer.cpp
 *
 *  Created on: Apr 17, 2017
 *      Author: jorge
 */

#include <iostream>
#include "LimitBuffer.h"
#include <memory>
#include <opencv2/core.hpp>
#include "DebugMat.h"


template<class T>
LimitBuffer<T>::LimitBuffer() {
	this->maxSize = 5;
}

template<class T>
LimitBuffer<T>::LimitBuffer(const int& size) {
	this->maxSize = size;
}

template<class T>
LimitBuffer<T>::~LimitBuffer() {}

template<class T>
void LimitBuffer<T>::enqueue(T obj) {
    std::lock_guard<std::mutex> lock(LimitBuffer<T>::frameMutex);
	while(q.size()>=this->maxSize){
		q.pop_front();
	}
	q.push_front(obj);
}

template<class T>
T LimitBuffer<T>::dequeue() {
    std::lock_guard<std::mutex> lock(LimitBuffer<T>::frameMutex);
	T res = q.back();
	q.pop_back();
	return res;
}

template<class T>
int LimitBuffer<T>::size() {
	return q.size();
}

template class LimitBuffer<std::shared_ptr<cv::Mat>>;
template class LimitBuffer<int>;
template class LimitBuffer<DebugMat>;
