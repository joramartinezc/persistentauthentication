/*******************************************************************************
 * Copyright 2017 Jorge Andres Martinez Castillo
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License.  You may obtain a copy
 * of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations under
 * the License.
 ******************************************************************************/
/*
 * Camera.h
 *
 *  Created on: Apr 14, 2017
 *      Author: jorge
 */

#ifndef FLOW_CAMERATRANSFORMATION_H_
#define FLOW_CAMERATRANSFORMATION_H_

#include <string>   // for strings
#include "Transformation.h"

#include <opencv2/core.hpp>     // Basic OpenCV structures (cv::Mat, Scalar)

class CameraTransformation: public Transformation {
public:
	CameraTransformation(const std::string& filename, double focalLenght, double xCenter, double yCenter, bool isFisheye);
	virtual ~CameraTransformation();

	cv::Mat apply(cv::Mat frame);

private:
	void loadParameters(const std::string& filename, cv::OutputArray map1, cv::OutputArray map2);
	double focalLenght;
	double xCenter;
	double yCenter;
	bool isFisheye;
	cv::Mat map1,map2;
};

#endif /* FLOW_CAMERATRANSFORMATION_H_ */
