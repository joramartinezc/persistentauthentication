////////////////////////////////////////////////////////////////////////////////
// Copyright 2017 Jorge Andres Martinez Castillo
// 
// Licensed under the Apache License, Version 2.0 (the "License"); you may not
// use this file except in compliance with the License.  You may obtain a copy
// of the License at
// 
//   http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
// WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
// License for the specific language governing permissions and limitations under
// the License.
////////////////////////////////////////////////////////////////////////////////
/*
 * Dilate.cpp
 *
 *  Created on: Apr 5, 2017
 *      Author: jorge
 */

#include "Dilate.h"

#include <opencv2/core.hpp>     // Basic OpenCV structures (cv::Mat, Scalar)
#include <opencv2/imgproc.hpp>
#include "../utils/Utils.h"

using namespace cv;

Dilate::Dilate() {
	dilateSize = 0;
	erodeSize = 0;
	cv::FileStorage fs;
	fs.open("settings/calibration.xml", cv::FileStorage::READ);
	fs["dilate_size"] >> dilateSize;
	fs["erode_size"] >> erodeSize;
	fs.release();

	Utils::log(std::vector<std::string>({"dilate_size: ", Utils::n2s(dilateSize), " erode_size: ",Utils::n2s(erodeSize)}));
}

Dilate::~Dilate() {
	// TODO Auto-generated destructor stub
}

cv::Mat Dilate::apply(cv::Mat frame) {

	Mat maskDil;
	// Apply the dilation operation
	//	cv::dilate(maskFilt, maskDil, Mat::ones(Size(20, 20), CV_8U));
	//	cv::erode(maskDil, maskDil, Mat::ones(Size(18, 18), CV_8U));
	cv::dilate(frame, maskDil, getStructuringElement(MORPH_ELLIPSE, Size(dilateSize, dilateSize)));
	cv::erode(maskDil, maskDil, getStructuringElement(MORPH_ELLIPSE, Size(erodeSize, erodeSize)));
	return maskDil;

}

