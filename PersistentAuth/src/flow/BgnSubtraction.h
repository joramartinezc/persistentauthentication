/*******************************************************************************
 * Copyright 2017 Jorge Andres Martinez Castillo
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License.  You may obtain a copy
 * of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations under
 * the License.
 ******************************************************************************/
/*
 * BgnSubtraction.h
 *
 *  Created on: Apr 5, 2017
 *      Author: jorge
 */

#ifndef SRC_FLOW_BGNSUBTRACTION_H_
#define SRC_FLOW_BGNSUBTRACTION_H_

#include <opencv2/core.hpp>     // Basic OpenCV structures (cv::Mat, Scalar)
#include <opencv2/core/cuda.hpp>     // Basic CUDA structures (cv::GpuMat)
#include <opencv2/video/background_segm.hpp>
#include "Transformation.h"


class BgnSubtraction: public Transformation {
public:
	BgnSubtraction();
	virtual ~BgnSubtraction(){}

	cv::Mat apply(cv::Mat frame);
private:
	cv::Ptr<cv::BackgroundSubtractor> pMOG; //MOG Background subtractor
	cv::cuda::GpuMat gpuFrame;
	cv::cuda::GpuMat gpuMask;
};

#endif /* SRC_FLOW_BGNSUBTRACTION_H_ */
