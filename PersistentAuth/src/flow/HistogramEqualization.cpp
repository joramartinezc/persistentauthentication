////////////////////////////////////////////////////////////////////////////////
// Copyright 2017 Jorge Andres Martinez Castillo
// 
// Licensed under the Apache License, Version 2.0 (the "License"); you may not
// use this file except in compliance with the License.  You may obtain a copy
// of the License at
// 
//   http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
// WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
// License for the specific language governing permissions and limitations under
// the License.
////////////////////////////////////////////////////////////////////////////////
/*
 * Denoise.cpp
 *
 *  Created on: Apr 5, 2017
 *      Author: jorge
 */

#include "HistogramEqualization.h"

#include <vector>

#include <opencv2/core.hpp>     // Basic OpenCV structures (cv::Mat, Scalar)
#include <opencv2/imgproc.hpp>
#include <opencv2/imgproc/imgproc.hpp>


HistogramEqualization::HistogramEqualization() {
	// TODO Auto-generated constructor stub
}

HistogramEqualization::~HistogramEqualization() {
	// TODO Auto-generated destructor stub
}

cv:: Mat HistogramEqualization::apply(cv::Mat frame) {
	cv::Mat ycrcb;//CV_BGR2YCrCb
	cv::cvtColor(frame,ycrcb,CV_BGR2HSV);

	std::vector<cv::Mat> channels;
	cv::split(ycrcb,channels);

	cv::equalizeHist(channels[2], channels[2]);

	cv::Mat result;
	cv::merge(channels,ycrcb);
	cv::cvtColor(ycrcb,result,CV_HSV2BGR);

	return result;
}

