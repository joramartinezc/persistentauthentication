////////////////////////////////////////////////////////////////////////////////
// Copyright 2017 Jorge Andres Martinez Castillo
// 
// Licensed under the Apache License, Version 2.0 (the "License"); you may not
// use this file except in compliance with the License.  You may obtain a copy
// of the License at
// 
//   http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
// WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
// License for the specific language governing permissions and limitations under
// the License.
////////////////////////////////////////////////////////////////////////////////
/*
 * Camera.cpp
 *
 *  Created on: Apr 14, 2017
 *      Author: jorge
 */

#include <iostream> // for standard I/O
#include <string>   // for strings

#include <opencv2/imgproc.hpp>
#include "CameraTransformation.h"
#include <opencv2/calib3d.hpp>

using namespace std;
using namespace cv;

void CameraTransformation::loadParameters(const string& filename, OutputArray map1, OutputArray map2) {
	FileStorage fs;
	fs.open( filename, FileStorage::READ );

	if( ! fs.isOpened() ){
		cerr << "Failed to open: " << filename << endl << flush;
	}

	Mat cameraMatrix(3,3,CV_32F);
	fs["camera_matrix"] >> cameraMatrix;
	Mat distCoeffs;
	fs["distortion_coefficients"] >> distCoeffs;
	float h,w;
	fs["image_width"] >> w;
	fs["image_height"] >> h;
	Size imgSize(w,h);

	Mat empty;
	double f = this->focalLenght;
	double data[10] = {f,0,xCenter,
					   0,f,yCenter,
					   0,0,1  };
	Mat newMat(3,3,CV_64F,data);
	if (isFisheye) {
//		Mat newMat;
//		fisheye::estimateNewCameraMatrixForUndistortRectify(cameraMatrix, distCoeffs, imgSize, Matx33d::eye(), newMat, 1);
		fisheye::initUndistortRectifyMap(cameraMatrix, distCoeffs, Matx33d::eye(), newMat, imgSize, CV_16SC2, map1, map2);
	} else {
//		newMat = getOptimalNewCameraMatrix(cameraMatrix, distCoeffs, imgSize, 1, imgSize, 0);
		initUndistortRectifyMap(cameraMatrix, distCoeffs, empty, newMat, imgSize, CV_16SC2, map1, map2);
	}

    fs.release();
}

CameraTransformation::CameraTransformation(const string& filename, double focalLenght, double xCenter, double yCenter, bool isFisheye) {
	this->focalLenght = focalLenght;
	this->xCenter = xCenter;
	this->yCenter = yCenter;
	this->isFisheye = isFisheye;
	loadParameters(filename, this->map1, this->map2);
}

CameraTransformation::~CameraTransformation() {}


cv::Mat CameraTransformation::apply(cv::Mat frame) {
	Mat remapedFrame;
	remap(frame, remapedFrame, map1, map2, INTER_CUBIC);
	return remapedFrame;
}
