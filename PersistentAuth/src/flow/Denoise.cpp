////////////////////////////////////////////////////////////////////////////////
// Copyright 2017 Jorge Andres Martinez Castillo
// 
// Licensed under the Apache License, Version 2.0 (the "License"); you may not
// use this file except in compliance with the License.  You may obtain a copy
// of the License at
// 
//   http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
// WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
// License for the specific language governing permissions and limitations under
// the License.
////////////////////////////////////////////////////////////////////////////////
/*
 * Denoise.cpp
 *
 *  Created on: Apr 5, 2017
 *      Author: jorge
 */

#include "Denoise.h"

#include <opencv2/core.hpp>     // Basic OpenCV structures (cv::Mat, Scalar)
#include <opencv2/imgproc.hpp>
#include "../utils/Utils.h"

using namespace cv;

Denoise::Denoise() {

	size = 0;
	cv::FileStorage fs;
	fs.open("settings/calibration.xml", cv::FileStorage::READ);
	fs["denoise_size"] >> size;
	fs.release();
	Utils::log(std::vector<std::string>({"denoise_size: ", Utils::n2s(size)}));
}

Denoise::~Denoise() {
	// TODO Auto-generated destructor stub
}

cv:: Mat Denoise::apply(cv::Mat frame) {
	Mat mask;
	cv::medianBlur(frame, mask, size);
	return mask;
}

