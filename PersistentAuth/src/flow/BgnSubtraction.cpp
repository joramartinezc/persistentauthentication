////////////////////////////////////////////////////////////////////////////////
// Copyright 2017 Jorge Andres Martinez Castillo
// 
// Licensed under the Apache License, Version 2.0 (the "License"); you may not
// use this file except in compliance with the License.  You may obtain a copy
// of the License at
// 
//   http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
// WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
// License for the specific language governing permissions and limitations under
// the License.
////////////////////////////////////////////////////////////////////////////////
/*
 * BgnSubtraction.cpp
 *
 *  Created on: Apr 5, 2017
 *      Author: jorge
 */

#include "BgnSubtraction.h"

#include <opencv2/core.hpp>     // Basic OpenCV structures (cv::Mat, Scalar)
#include <opencv2/video/background_segm.hpp>
#include <opencv2/bgsegm.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <cudabgsegm.hpp>
#include "../utils/Utils.h"

//#define SRC_FLOW_BGNSUBTRACTION_H_DEBUG

#ifdef SRC_FLOW_BGNSUBTRACTION_H_DEBUG
//debug includes
#include <thread>
#include <sstream>  // string to number conversion
#include "../utils/Utils.h"
#include <vector>
#include <string>
#include "../utils/DebugWin.h"
#endif

BgnSubtraction::BgnSubtraction() {

	cv::FileStorage fs;
	fs.open("settings/calibration.xml", cv::FileStorage::READ);
	int history;
	fs["MOG2_history"] >> history;
	int varThreshold;
	fs["MOG2_varThreshold"] >> varThreshold;
	fs.release();

	Utils::log(std::vector<std::string>({"hist: ", Utils::n2s(history), " var: ",Utils::n2s(varThreshold)}));

//	pMOG = cv::createBackgroundSubtractorMOG2(5000, 64, true);
//	pMOG = cv::bgsegm::createBackgroundSubtractorGMG(15, 0.9999);
//	pMOG = cv::createBackgroundSubtractorKNN(500,400.0,true);
//	pMOG = cv::createBackgroundSubtractorMOG2(5000, 64, true);
	pMOG = cv::cuda::createBackgroundSubtractorMOG2(history, varThreshold, true);
	gpuFrame = cv::cuda::GpuMat();
	gpuMask = cv::cuda::GpuMat();
}

cv::Mat BgnSubtraction::apply(cv::Mat frame) {
		cv::Mat mask;
		gpuFrame.upload(frame);
		gpuMask.upload(mask);
		this->pMOG->apply(gpuFrame, gpuMask, 0.001);
		gpuMask.download(mask);
		//	cv::Mat kernel = cv::getStructuringElement(cv::MORPH_ELLIPSE, cv::Size(3, 3));
		//	cv::morphologyEx(mask, mask, cv::MORPH_OPEN, kernel);

		#ifdef SRC_FLOW_BGNSUBTRACTION_H_DEBUG
		cv::cuda::GpuMat gpuBg;
		cv::Mat bg;
		pMOG->getBackgroundImage(gpuBg);
		std::ostringstream win;
		win << "BG: ";
		win << std::this_thread::get_id();
		gpuBg.download(bg);
		DebugWin::getInstance().enqueDebug(win.str(),bg);
		#endif

		return mask;
}
