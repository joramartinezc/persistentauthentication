////////////////////////////////////////////////////////////////////////////////
// Copyright 2017 Jorge Andres Martinez Castillo
// 
// Licensed under the Apache License, Version 2.0 (the "License"); you may not
// use this file except in compliance with the License.  You may obtain a copy
// of the License at
// 
//   http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
// WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
// License for the specific language governing permissions and limitations under
// the License.
////////////////////////////////////////////////////////////////////////////////
/*
 * AkazeId.cpp
 *
 *  Created on: Apr 5, 2017
 *      Author: jorge
 */

#include "AkazeId.h"
#include "../utils/Utils.h"

#include <iostream>
#include <string>
#include <opencv2/imgproc.hpp>
#include <opencv2/highgui.hpp>
#include <opencv2/features2d.hpp>
#include <opencv2/xfeatures2d.hpp>

const float inlier_threshold = 2.5f; // Distance threshold to identify inliers
const float nn_match_ratio = 0.8f;   // Nearest neighbor matching ratio

AkazeId::AkazeId() {}

AkazeId::~AkazeId() {}

void AkazeId::init(const Frame& frame, const cv::Mat& mask, const cv::RotatedRect& boundRect) {

	cv::Mat greyMat;
	cv::cvtColor(*frame.getFrameMat().get(), greyMat, CV_BGR2GRAY);

	cv::Mat mask2 = cv::Mat::zeros(mask.rows, mask.cols, CV_8UC1);
	Utils::rotatedRectangle(mask2,boundRect, Scalar(255), 1, LINE_8, 0, true);
	mask2 = mask2.mul(mask);

	akaze = cv::AKAZE::create();
//	akaze = xfeatures2d::SIFT::create();
	akaze->detectAndCompute(greyMat, mask2, keyPntsOrig, descriptorsOrig);
//	imwrite("o_mask.png",mask2);
//	imwrite("orig.png",*frame.getFrameMat().get());
}

std::shared_ptr<CredResult> AkazeId::checkSimilarity(const Frame& frame, const cv::Mat& mask, const cv::RotatedRect& boundRect, cv::Point2f grndPoint) {

	cv::Mat greyMat;
	cv::cvtColor(*frame.getFrameMat().get(), greyMat, cv::COLOR_BGR2GRAY);
	cv::Mat mask2 = cv::Mat::zeros(mask.rows, mask.cols, CV_8UC1);
	Utils::rotatedRectangle(mask2,boundRect, Scalar(255), 1, LINE_8, 0, true);
	mask2 = mask2.mul(mask);

	std::vector<cv::KeyPoint> keyPoints;
	cv::Mat descriptors;
	akaze->detectAndCompute(greyMat, mask2, keyPoints, descriptors);

	if(descriptorsOrig.type()!=CV_32F) {
		descriptorsOrig.convertTo(descriptorsOrig, CV_32F);
	}
	if(descriptors.type()!=CV_32F) {
		descriptors.convertTo(descriptors, CV_32F);
	}

	cv::FlannBasedMatcher matcher;
//	BFMatcher matcher(NORM_HAMMING);
	std::vector<std::vector<cv::DMatch> > nn_matches;

//	imwrite("d_mask.png",mask2);
//	imwrite("dest.png",*frame.getFrameMat().get());
	int MIN_MATCHES = 2;

	double inlierRatio = 0;
	if(descriptors.rows >= MIN_MATCHES){
		matcher.knnMatch( descriptorsOrig, descriptors, nn_matches, MIN_MATCHES);

			std::vector<cv::KeyPoint> matched1, matched2, inliers1, inliers2;
			std::vector<cv::DMatch> good_matches;
			for (size_t i = 0; i < nn_matches.size(); i++) {
				cv::DMatch first = nn_matches[i][0];
				float dist1 = nn_matches[i][0].distance;
				float dist2 = nn_matches[i][1].distance;

				if (dist1 < nn_match_ratio * dist2) {
					matched1.push_back(keyPntsOrig[first.queryIdx]);
					matched2.push_back(keyPoints[first.trainIdx]);
				}
			}

			for (unsigned i = 0; i < matched1.size(); i++) {
				cv::Mat col = cv::Mat::ones(3, 1, CV_64F);
				col.at<double>(0) = matched1[i].pt.x;
				col.at<double>(1) = matched1[i].pt.y;

		//		col = homography * col;
				col /= col.at<double>(2);
				double dist = sqrt(pow(col.at<double>(0) - matched2[i].pt.x, 2) + pow(col.at<double>(1) - matched2[i].pt.y, 2));

				if (dist < inlier_threshold) {
					int new_i = static_cast<int>(inliers1.size());
					inliers1.push_back(matched1[i]);
					inliers2.push_back(matched2[i]);
					good_matches.push_back(cv::DMatch(new_i, new_i, 0));
				}
			}

			inlierRatio = inliers1.size() * 1.0 / matched1.size();
	}
	std::shared_ptr<CredResult> res = std::make_shared<CredResult>(inlierRatio);
	return res;
}

void AkazeId::confirm(std::shared_ptr<CredResult> result){}
