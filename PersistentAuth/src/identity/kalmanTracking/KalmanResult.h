/*******************************************************************************
 * Copyright 2017 Jorge Andres Martinez Castillo
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License.  You may obtain a copy
 * of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations under
 * the License.
 ******************************************************************************/
/*
 * KalmanResult.h
 *
 *  Created on: May 3, 2017
 *      Author: jorge
 */

#ifndef IDENTITY_KALMANTRACKING_KALMANRESULT_H_
#define IDENTITY_KALMANTRACKING_KALMANRESULT_H_

#include <opencv2/core.hpp>

#include "../CredResult.h"

class KalmanResult : public CredResult {
public:
	KalmanResult(double result, cv::Mat measurement, cv::Mat prediction, double msec);
	virtual ~KalmanResult();
	const cv::Mat& getMeasurement() const;
	double getMsec() const;
	const cv::Mat& getPrediction() const;

private:
	cv::Mat prediction;
	cv::Mat measurement;
	double msec;
};

#endif /* IDENTITY_KALMANTRACKING_KALMANRESULT_H_ */
