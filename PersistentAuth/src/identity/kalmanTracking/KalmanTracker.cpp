////////////////////////////////////////////////////////////////////////////////
// Copyright 2017 Jorge Andres Martinez Castillo
// 
// Licensed under the Apache License, Version 2.0 (the "License"); you may not
// use this file except in compliance with the License.  You may obtain a copy
// of the License at
// 
//   http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
// WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
// License for the specific language governing permissions and limitations under
// the License.
////////////////////////////////////////////////////////////////////////////////
/*
 * KalmanTracking.cpp
 *
 *  Created on: Apr 5, 2017
 *      Author: jorge
 */

#include "../../utils/Utils.h"
#include "KalmanResult.h"

#include <memory>
#include <iostream>
#include <string>
#include <opencv2/imgproc.hpp>
#include <opencv2/highgui.hpp>
#include "KalmanTracker.h"

KalmanTracker::KalmanTracker() {
}

KalmanTracker::~KalmanTracker() {
}

void KalmanTracker::init(double initTick, cv::Point2f grndPoint) {
	//>>>> KF init
	int stateSize = 4;
	int measSize = 2;
	KF = KalmanFilter(stateSize, measSize, 0);

	/* 1 0 1 0     X pos
	 * 0 1 0 1     Y pos
	 * 0 0 1 0     x speed
	 * 0 0 0 1     Y speed  */
//		KF.transitionMatrix = Mat::eye(stateSize, stateSize, CV_32F);
	KF.transitionMatrix = Mat::eye(KF.transitionMatrix.rows, KF.transitionMatrix.cols, CV_32F);
	KF.transitionMatrix.at<float>(2) = 1.0f;
	KF.transitionMatrix.at<float>(7) = 1.0f;

//	KF.statePre = Mat::zeros(Size(1, stateSize),CV_32F);
	// Measure Matrix H
	// [ 1 0 0 0 ]
	// [ 0 1 0 0 ]
//		KF.measurementMatrix = cv::Mat::zeros(measSize, stateSize, CV_32F);
	KF.measurementMatrix = cv::Mat::zeros(KF.measurementMatrix.rows, KF.measurementMatrix.cols, CV_32F);
	KF.measurementMatrix.at<float>(0) = 1.0f;
	KF.measurementMatrix.at<float>(5) = 1.0f;
	//<<<< KF init

//	setIdentity(KF.processNoiseCov, Scalar::all(8e-3));
//	setIdentity(KF.errorCovPost, Scalar::all(.1));

	KF.statePre.at<float>(0) = grndPoint.x;
	KF.statePre.at<float>(1) = grndPoint.y;
	KF.statePre.at<float>(2) = 0;
	KF.statePre.at<float>(3) = 0;

	Mat measurement(Size(1, KF.measurementMatrix.rows), CV_32F);
	measurement.at<float>(0) = grndPoint.x;
	measurement.at<float>(1) = grndPoint.y;
	KF.correct(measurement);

	lastTick = initTick;
}

std::shared_ptr<KalmanResult> KalmanTracker::checkSimilarity(double tick, cv::Point2f grndPoint) {

	double precTick = lastTick;
	double dT = (tick - precTick) / 1000; //seconds
	if (dT >= 5) { //if at least 5 seconds have passed
		dT = 0; //speed zero from last confirmed measurement
	}
	KF.transitionMatrix.at<float>(2) = dT;
	KF.transitionMatrix.at<float>(7) = dT;

	Mat prediction = KF.predict();
	Mat measurement(Size(1, KF.measurementMatrix.rows), CV_32F);
	measurement.at<float>(0) = grndPoint.x;
	measurement.at<float>(1) = grndPoint.y;

	Mat pred2(Size(1, KF.measurementMatrix.rows), CV_32F);
	pred2.at<float>(0) = prediction.at<float>(0);
	pred2.at<float>(1) = prediction.at<float>(1);
	double distance = cv::norm(pred2, measurement);

	double nDist = distance / 300; //600 cm
	if (nDist < 0.5) {
//		Utils::log(std::vector<std::string>({"KalDist: ", Utils::n2s(nDist,5)}));
	}
	nDist = nDist >= 1 ? 1 : nDist;
	nDist = nDist <= 0 ? 0 : nDist;
	nDist = nDist * 1;

	std::shared_ptr<KalmanResult> res = std::make_shared<KalmanResult>(nDist, measurement, pred2, tick);
	return res;
}

void KalmanTracker::confirm(std::shared_ptr<KalmanResult> result) {
	std::shared_ptr<KalmanResult> kres = std::static_pointer_cast<KalmanResult>(result);
	lastTick = kres->getMsec();
	KF.correct(kres->getMeasurement());
}
