/*******************************************************************************
 * Copyright 2017 Jorge Andres Martinez Castillo
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License.  You may obtain a copy
 * of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations under
 * the License.
 ******************************************************************************/
/*
 * KalmanTracking.h
 *
 *  Created on: Apr 5, 2017
 *      Author: jorge
 */
#include <opencv2/core.hpp>     // Basic OpenCV structures (cv::Mat, Scalar, Rect)
#include <opencv2/video/tracking.hpp>
#include <memory>

#include "KalmanResult.h"

#ifndef SRC_KALMANTRACKING_H_
#define SRC_KALMANTRACKING_H_

using namespace cv;

class KalmanTracker {
public:
	KalmanTracker();
	virtual ~KalmanTracker();

	std::shared_ptr<KalmanResult> checkSimilarity(double tick, cv::Point2f grndPoint);
	void confirm(std::shared_ptr<KalmanResult> result);
	void init(double initialTick, cv::Point2f grndPoint);

private:
	KalmanFilter KF;
	double lastTick;
};

#endif /* SRC_HISTOGRAMID_H_ */
