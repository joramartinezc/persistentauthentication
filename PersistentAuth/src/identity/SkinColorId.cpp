////////////////////////////////////////////////////////////////////////////////
// Copyright 2017 Jorge Andres Martinez Castillo
// 
// Licensed under the Apache License, Version 2.0 (the "License"); you may not
// use this file except in compliance with the License.  You may obtain a copy
// of the License at
// 
//   http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
// WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
// License for the specific language governing permissions and limitations under
// the License.
////////////////////////////////////////////////////////////////////////////////
/*
 * KalmanTracking.cpp
 *
 *  Created on: Apr 5, 2017
 *      Author: jorge
 */

#include "SkinColorId.h"

#include "../utils/Utils.h"
#include "kalmanTracking/KalmanResult.h"
#include "kalmanTracking/KalmanTracker.h"

#include <memory>
#include <iostream>
#include <string>
#include <opencv2/imgproc.hpp>
#include <opencv2/highgui.hpp>
#include <opencv2/calib3d.hpp>

const cv::Scalar skinLow =  cv::Scalar(0,   133, 77);
const cv::Scalar skinHigh = cv::Scalar(255, 173, 127);
const double maxDist = cv::norm(Scalar(0,skinLow.val[1],skinLow.val[2]), cv::Scalar(0,skinHigh.val[1],skinHigh.val[2]));

SkinColorId::SkinColorId() {}

SkinColorId::~SkinColorId() {}

cv::Scalar getColor(const Frame& frameObj, const cv::Mat& mask, const cv::RotatedRect& boundRect, cv::Point2f grndPoint) {

	const Mat& frame = *(frameObj.getFrameMat().get());
	Point2f pos = grndPoint;

	//Calculate Person orientation
	Point2f pnts[4];
	boundRect.points(pnts);
	float maxDist[] = {0,0};
	Point2f farPnts[] = {Point2f(0,0),Point2f(0,0)};

	for (int i = 0; i < 4; ++i) {
		Point2f p = pnts[i];
		float xDif = p.x-pos.x;
		float yDif = p.y-pos.y;

		float dist = sqrtf(xDif*xDif+yDif*yDif);
		if(dist > maxDist[0]) {
			maxDist[1] = maxDist[0];
			maxDist[0] = dist;

			farPnts[1] = farPnts[0];
			farPnts[0] = p;

		} else if(dist > maxDist[1]) {
			maxDist[1] = dist;

			farPnts[1] = p;
		}
	}

//	cv::line(*result.get(),farPnts[0],farPnts[1],Scalar(0,255,0),2);

	float m1 = (farPnts[0].y - farPnts[1].y)/(farPnts[0].x - farPnts[1].x);
	Point2f p2;
	for (int i = 0; i < 4; ++i) {
		Point2f p = pnts[i];
		if(p != farPnts[0] && p != farPnts[1]) {
			float xDif = p.x-farPnts[0].x;
			float yDif = p.y-farPnts[0].y;

			float m2 = yDif/xDif;

			if(m1*m2 < -0.99 && m1*m2 > -1.01) {
				float rat = 2;
				p2 = Point2f(p.x-xDif/rat, p.y-yDif/rat);
				Point2f _point1 = p2;
				Point2f _point2 = farPnts[0];
				Point2f _point3 = farPnts[1];
			    Vec2f vecs[2];
			    vecs[0] = Vec2f(_point1 - _point2);
			    vecs[1] = Vec2f(_point2 - _point3);
			    // check that given sides are perpendicular
			    float pp = abs(vecs[0].dot(vecs[1])) / (norm(vecs[0]) * norm(vecs[1]));

			    Point2f _center = 0.5f * (_point1 + _point3);
			    int wd_i = 0;
			    if( abs(vecs[1][1]) < abs(vecs[1][0]) ) wd_i = 1;
			    int ht_i = (wd_i + 1) % 2;

			    float _angle = atan(vecs[wd_i][1] / vecs[wd_i][0]) * 180.0f / (float) CV_PI;
			    float _width = (float) norm(vecs[wd_i]);
			    float _height = (float) norm(vecs[ht_i]);

				RotatedRect rec = RotatedRect(_center,Size2f(_width,_height),_angle);
//						Utils::rotatedRectangle(*result.get(),rec,Scalar(125,125,0),2);

			    float angle = rec.angle;
			    Size rectSize = rec.size;

				if (rec.angle < -45.) {
					angle += 90.0;
					swap(rectSize.width, rectSize.height);
				}
				// get the rotation matrix
				Mat R = getRotationMatrix2D(rec.center, angle, 1.0);
			    // perform the affine transformation
				Mat face, frameBlobCut;
		        frame.copyTo(frameBlobCut,mask);
				//rotate and crop according to bounding box
			    cv::warpAffine(frameBlobCut, face, R, frame.size(), cv::INTER_CUBIC);
			    cv::getRectSubPix(face, rectSize, rec.center, face);

				Mat yrb_face;
				cv::cvtColor(face, yrb_face, COLOR_BGR2YCrCb);

				Mat skinMask;
				cv::inRange(yrb_face,skinLow, skinHigh, skinMask);

				// Use a rectangle representation on the frame but warp back the coordinates
				// Retrieve the 4 corners detected in the rotation image
				Point p1 ( rec.center.x-rec.size.height/2., rec.center.y-rec.size.width/2. ); // Top left
				Point p2 ( rec.center.x+rec.size.height/2., rec.center.y-rec.size.width/2. ); // Top right
				Point p3 ( rec.center.x+rec.size.height/2., rec.center.y+rec.size.width/2. ); // Down right
				Point p4 ( rec.center.x-rec.size.height/2., rec.center.y+rec.size.width/2. ); // Down left

				// Add the 4 points to a matrix structure
				Mat coordinates = (Mat_<double>(3,4) << p1.x, p2.x, p3.x, p4.x,\
				                                        p1.y, p2.y, p3.y, p4.y,\
				                                        1   , 1  ,  1   , 1    );

				// Apply a new inverse tranformation matrix
				Point2f pt(frame.cols/2., frame.rows/2.);
//						Mat r = getRotationMatrix2D(pt, -rec.angle, 1.0);
				Mat r = getRotationMatrix2D(rec.center, -angle, 1.0);
				Mat res = r * coordinates;

				// Retrieve the ew coordinates from the tranformed matrix
				Point p1_back, p2_back, p3_back, p4_back;
				p1_back.x=(int)res.at<double>(0,0);
				p1_back.y=(int)res.at<double>(1,0);

				p2_back.x=(int)res.at<double>(0,1);
				p2_back.y=(int)res.at<double>(1,1);

				p3_back.x=(int)res.at<double>(0,2);
				p3_back.y=(int)res.at<double>(1,2);

				p4_back.x=(int)res.at<double>(0,3);
				p4_back.y=(int)res.at<double>(1,3);

				std::vector<Point2f> src,dst;
				src.push_back(Point2f(0,0));
				src.push_back(Point2f(skinMask.cols,0));
				src.push_back(Point2f(skinMask.cols,skinMask.rows));
				src.push_back(Point2f(0,skinMask.rows));
				dst.push_back(p1_back);
				dst.push_back(p2_back);
				dst.push_back(p3_back);
				dst.push_back(p4_back);
				Mat hom = cv::findHomography( src, dst, CV_RANSAC );

//				cv::imwrite("face.jpg",face);
//				cv::imwrite("mask.jpg",skinMask);

				Mat skinMaskFrame;
				cv::warpPerspective(skinMask,skinMaskFrame,hom,frame.size());

				Scalar skinColor = cv::mean( face, skinMask );
//				cout << skinColor << endl << flush;

//				cv::imwrite("skinMaskFrame.jpg",skinMaskFrame);
//				cv::imwrite("frame.jpg",*result.get());
//		        cv::add(*result.get(), Scalar(0, 100, 100), *result.get(), skinMaskFrame);

				return skinColor;
			}
		}
	}
}


void SkinColorId::init(const Frame& frame, const cv::Mat& mask, const cv::RotatedRect& boundRect, cv::Point2f grndPoint) {
	this->color = getColor(frame, mask, boundRect, grndPoint);
}

std::shared_ptr<CredResult> SkinColorId::checkSimilarity(const Frame& frame, const cv::Mat& mask, const cv::RotatedRect& boundRect, cv::Point2f grndPoint) {

	cv::Scalar measuredColor = getColor(frame, mask, boundRect, grndPoint);

	cv::Scalar colorN = cv::Scalar(0,color.val[1],color.val[2]);
	cv::Scalar measuredColorN = cv::Scalar(0,measuredColor.val[1],measuredColor.val[2]);

	double dist = cv::norm(colorN, measuredColorN);
	std::shared_ptr<CredResult> res = std::make_shared<CredResult>(dist/maxDist);

	return res;
}

void SkinColorId::confirm(std::shared_ptr<CredResult> result){}
