/*******************************************************************************
 * Copyright 2017 Jorge Andres Martinez Castillo
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License.  You may obtain a copy
 * of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations under
 * the License.
 ******************************************************************************/
/*
 * AkazeId.h
 *
 *  Created on: Apr 5, 2017
 *      Author: jorge
 */
#include <opencv2/core.hpp>     // Basic OpenCV structures (cv::Mat, Scalar, Rect)
#include <opencv2/features2d.hpp>

#ifndef SRC_AKAZEID_H_
#define SRC_AKAZEID_H_

#include "../entities/Frame.h"


#include "Credential.h"
#include "CredResult.h"

class AkazeId: public Credential {
public:
	AkazeId();
	virtual ~AkazeId();

	void init(const Frame& frame, const cv::Mat& mask, const cv::RotatedRect& boundRect);
	std::shared_ptr<CredResult> checkSimilarity(const Frame& frame, const cv::Mat& mask, const cv::RotatedRect& boundRect, cv::Point2f grndPoint);
	void confirm(std::shared_ptr<CredResult> result);

private:
	cv::Ptr<cv::Feature2D> akaze;
	std::vector<cv::KeyPoint> keyPntsOrig;
	cv::Mat descriptorsOrig;

};

#endif /* SRC_AKAZEID_H_ */
