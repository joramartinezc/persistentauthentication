////////////////////////////////////////////////////////////////////////////////
// Copyright 2017 Jorge Andres Martinez Castillo
// 
// Licensed under the Apache License, Version 2.0 (the "License"); you may not
// use this file except in compliance with the License.  You may obtain a copy
// of the License at
// 
//   http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
// WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
// License for the specific language governing permissions and limitations under
// the License.
////////////////////////////////////////////////////////////////////////////////
/*
 * Principal.cpp
 *
 *  Created on: Apr 5, 2017
 *      Author: jorge
 */

#include "Principal.h"
#include <string>
#include <vector>

Principal::Principal() {}

Principal::~Principal() {}

const std::shared_ptr<std::vector<std::shared_ptr<Credential>>> Principal::getCredentials() const {
	return credentials;
}

void Principal::setCredentials(std::shared_ptr<std::vector<std::shared_ptr<Credential>>> credentials) {
	this->credentials = credentials;
}


const std::string& Principal::getPrincipalId() const {
	return principalId;
}

void Principal::setPrincipalId(std::string p) {
	this->principalId = p;
}

