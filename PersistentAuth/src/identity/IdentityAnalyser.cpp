////////////////////////////////////////////////////////////////////////////////
// Copyright 2017 Jorge Andres Martinez Castillo
// 
// Licensed under the Apache License, Version 2.0 (the "License"); you may not
// use this file except in compliance with the License.  You may obtain a copy
// of the License at
// 
//   http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
// WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
// License for the specific language governing permissions and limitations under
// the License.
////////////////////////////////////////////////////////////////////////////////
///*
// * IdentityAnalyser.cpp
// *
// *  Created on: Apr 6, 2017
// *      Author: jorge
// */
//
#include "IdentityAnalyser.h"

#include <list>
#include <vector>
#include <string>
#include <iterator>
#include <memory>
#include <iostream>
#include <limits>

#include "Credential.h"
#include "HistogramId.h"
#include "../sceneModel/Environment.h"
#include "../sceneModel/Zone.h"
#include "../utils/Utils.h"
#include "kalmanTracking/KalmanTracker.h"
#include "kalmanTracking/KalmanResult.h"
#include "KalmanId.h"
#include "AkazeId.h"
#include "SkinColorId.h"

std::map<std::string, std::shared_ptr<Principal>> IdentityAnalyser::ppals;
double IdentityAnalyser::blobAreaRate = 0;
IdentityAnalyser::_init IdentityAnalyser::staticConstructor;

IdentityAnalyser::IdentityAnalyser() {}

IdentityAnalyser::~IdentityAnalyser() {}

const std::map<std::string, std::shared_ptr<Principal> >& IdentityAnalyser::getPpals() {
	return ppals;
}

void IdentityAnalyser::printResult(std::shared_ptr<CredResult> results, const Frame& frame, Camera& camera, Scalar color, Point2f grndPoint) {
	if (std::shared_ptr<KalmanResult> kRes = std::dynamic_pointer_cast<KalmanResult>(results)) {
		const cv::Mat pred2 = kRes->getPrediction();
		float x = pred2.at<float>(0);
		float y = pred2.at<float>(1);
		Point2f frmPnt = camera.realToFrame(std::vector<Point2f> { Point2f(x, y) })[0];
		cv::circle(*frame.getFrameMat().get(), frmPnt, 10, color, -1, LINE_8);
	} else {
		cv::putText(*frame.getFrameMat().get(), Utils::n2s(results->getResult()), grndPoint, FONT_HERSHEY_DUPLEX, 1, color);
	}

}

std::vector<std::shared_ptr<Blob>> IdentityAnalyser::createBlobs(Camera& camera, const Frame& frame,const cv::Mat& mask,const vector<RotatedRect>& boundingBoxes, int idZone, std::shared_ptr<Principal> principal) {

	double blobArea = frame.getFrameMat()->rows * frame.getFrameMat()->cols/IdentityAnalyser::blobAreaRate;
	//Create Blob objects
	std::vector<std::shared_ptr<Blob>> blobs;
	for (unsigned i = 0; i < boundingBoxes.size(); i++) {
		RotatedRect boundingBox = boundingBoxes[i];
		if (boundingBox.size.area() > blobArea) {
			std::vector<shared_ptr<Principal>> blobPpalVec;
			// Get ground position from blob
			Point2f grndPoint = camera.getGroundPoint(boundingBox);
			Point2f grndPointReal = camera.frameToReal(std::vector<Point2f>{grndPoint})[0];
			std::shared_ptr<Blob> blob = std::make_shared<Blob>(blobPpalVec, boundingBox, grndPoint);
			blobs.push_back(blob);

			//If authenticating a blob in the right zone, assume that is the initial position for the principal
			if (idZone != -1 && principal != nullptr) {
				Environment env = Environment::getInstance();
				std::shared_ptr<Zone> zone = env.getZone(idZone);
				if(zone->containsPoint(grndPointReal)) {
					Utils::log(vector<std::string>({"###Detected: ", principal->getPrincipalId()," @ (",Utils::n2s(grndPointReal.x,4), ", ", Utils::n2s(grndPointReal.y,4), "), ", Utils::n2s(frame.getMsec(),1), " msec"}));
					system(("echo "+principal->getPrincipalId()+" | festival --tts &").c_str());
					if ( ppals.count(principal->getPrincipalId()) > 0 ) {
						principal = ppals[principal->getPrincipalId()];
					}
					std::shared_ptr<HistogramId> hist = std::make_shared<HistogramId>();
					hist->init(frame, mask, boundingBox, grndPointReal);

					std::shared_ptr<KalmanTracker> tracker = std::make_shared<KalmanTracker>();
					tracker->init(frame.getMsec(),grndPointReal);
					std::shared_ptr<KalmanId> kTrack = std::make_shared<KalmanId>();
					kTrack->init(frame, grndPointReal,tracker);

					std::shared_ptr<AkazeId> akaze = std::make_shared<AkazeId>();
					akaze->init(frame, mask, boundingBox);

					std::shared_ptr<SkinColorId> skinId = std::make_shared<SkinColorId>();
					skinId->init(frame, mask, boundingBox, grndPointReal);

					std::shared_ptr<std::vector<std::shared_ptr<Credential>>> credentials = std::make_shared<std::vector<std::shared_ptr<Credential>>>();//{hist,kTrack}
					credentials->push_back(hist);
					credentials->push_back(kTrack);
//					credentials->push_back(akaze);
//					credentials->push_back(skinId);
					principal->setCredentials(credentials);
					ppals[principal->getPrincipalId()] = principal;
				}
			}
		}
	}/// End create blobs

	std::vector<std::shared_ptr<CredResult>> resBlob[blobs.size()];
	// For each principal
	for (auto& ppalE : ppals) {
		std::shared_ptr<Principal> ppal = ppalE.second;

		/// Search min scored blob-principal below threshold
		int minScoreIdx = -1;
		double minScore = std::numeric_limits<double>::max();
		std::shared_ptr<std::vector<std::shared_ptr<CredResult>>> minScoreResults;
		// For each blob
		for (unsigned i = 0; i < blobs.size(); ++i) {
			auto& blob = blobs[i];
			RotatedRect boundingBox = blob->getBoundingBox();
			if (boundingBox.size.area() > blobArea) {

				// Get ground position from blob
				Point2f grndPoint = blob->getPosition();
				Point2f grndPointReal = camera.frameToReal(std::vector<Point2f>{grndPoint})[0];

				/// Calculate principal-blob score (average of credentials)
				const std::shared_ptr<std::vector<std::shared_ptr<Credential>>> creds = ppal->getCredentials();
				double similarity = 0;
				std::vector<std::shared_ptr<CredResult>> results;
				// For each credential
				for (unsigned k = 0; k < creds->size(); k++) {
					std::shared_ptr<Credential> credential = creds->at(k);
					std::shared_ptr<CredResult> res = credential->checkSimilarity(frame, mask, boundingBox, grndPointReal);
					//sum result to score
					similarity += res->getResult();
					results.push_back(res);//results[k] = res, same index than credentials
				}
				//divide on the amount of added elements for averaged similarity
				similarity = similarity / creds->size();

				//save new found min score
				if(similarity < minScore) {
					minScore = similarity;
					minScoreIdx = i;
					minScoreResults = std::make_shared<std::vector<std::shared_ptr<CredResult>>>(results);
				}
				resBlob[i] = results;
			}
		}///End search min scored blob

		//print other results
		for (unsigned i = 0; i < blobs.size(); ++i) {
//			if (minScoreIdx != k) {
				auto& blob = blobs[i];
				// Get ground position from blob
				Point2f grndPoint = blob->getPosition();
				const auto& creds = ppal->getCredentials();
				for (unsigned k = 0; k < creds->size(); k++) {
					//print result for debug
//					double score = resBlob[i].at(k)->getResult();
//					Utils::log(std::vector<string> {"- SCORE",Utils::n2s(k,1),": ", Utils::n2s(score,1) });
					Scalar color = CV_RGB(100, 100, 100);
					IdentityAnalyser::printResult(resBlob[i].at(k), frame, camera, color, grndPoint);
				}
//			}
		}

		// If score is below threshold
		if (minScore < 0.4) {
			auto& blob = blobs[minScoreIdx];
//			Utils::log(std::vector<string> {"Y:",Utils::n2s(grndPoint.y,1), " SIMILARITY: ", Utils::n2s(similarity,1) , " POS: ", Utils::n2s(grndPointReal.x,1), ",", Utils::n2s(grndPointReal.y,1) });
			// Assign principal to blob
			blob->getPrincipals().push_back(ppal);
			// Get ground position from blob
			Point2f grndPoint = blob->getPosition();
			// Confirm credential
			const auto& creds = ppal->getCredentials();
			for (unsigned k = 0; k < creds->size(); k++) {
				(*creds)[k]->confirm(minScoreResults->at(k));

				//print result for debug
//				double score = minScoreResults->at(k)->getResult();
//				Utils::log(std::vector<string> {"+ SCORE",Utils::n2s(k,1),": ", Utils::n2s(score,1) });
				Scalar color = CV_RGB(0, 0, 255);
				IdentityAnalyser::printResult(minScoreResults->at(k), frame, camera, color, grndPoint);
			}
		}
	}

	return blobs;
}
