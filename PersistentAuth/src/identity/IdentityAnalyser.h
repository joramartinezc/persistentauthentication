/*******************************************************************************
 * Copyright 2017 Jorge Andres Martinez Castillo
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License.  You may obtain a copy
 * of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations under
 * the License.
 ******************************************************************************/
/*
 * IdentityAnalyser.h
 *
 *  Created on: Apr 6, 2017
 *      Author: jorge
 */

#ifndef SRC_IDENTITYANALYSER_H_
#define SRC_IDENTITYANALYSER_H_
#include "Blob.h"
#include <vector>
#include <map>

//class Camera;
#include "../sceneModel/Camera.h"
#include "../entities/Frame.h"
#include "../utils/Utils.h"

class IdentityAnalyser {
public:
	IdentityAnalyser();
	virtual ~IdentityAnalyser();

	static std::vector<std::shared_ptr<Blob>>createBlobs(Camera& camera, const Frame& frame, const Mat& mask, const vector<RotatedRect>& boundingBoxes, int idZone, std::shared_ptr<Principal> principal);
	static const std::map<std::string, std::shared_ptr<Principal> >& getPpals();

private:
	static class _init {
	public:
		_init(){

			cv::FileStorage fs;
			fs.open("settings/calibration.xml", cv::FileStorage::READ);
			fs["ID_blob_area_rate"] >> IdentityAnalyser::blobAreaRate;
			fs.release();

			Utils::log(std::vector<std::string>({"ID_blob_area_rate: ", Utils::n2s(IdentityAnalyser::blobAreaRate)}));
		}
	} _initializer;
    friend class _init;
	static _init staticConstructor;

	static double blobAreaRate;

	static std::map<std::string, std::shared_ptr<Principal>> ppals;

	static void printResult(std::shared_ptr<CredResult> results, const Frame& frame, Camera& camera, Scalar color, Point2f grndPoint);
};

#endif /* SRC_IDENTITYANALYSER_H_ */
