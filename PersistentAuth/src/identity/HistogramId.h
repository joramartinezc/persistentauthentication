/*******************************************************************************
 * Copyright 2017 Jorge Andres Martinez Castillo
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License.  You may obtain a copy
 * of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations under
 * the License.
 ******************************************************************************/
/*
 * HistogramId.h
 *
 *  Created on: Apr 5, 2017
 *      Author: jorge
 */
#include <opencv2/core.hpp>     // Basic OpenCV structures (cv::Mat, Scalar, Rect)

#ifndef SRC_HISTOGRAMID_H_
#define SRC_HISTOGRAMID_H_

using namespace cv;

#include "../entities/Frame.h"

#include "Credential.h"
#include "CredResult.h"

class HistogramId: public Credential {
public:
	HistogramId();
	virtual ~HistogramId();

	std::shared_ptr<CredResult> checkSimilarity(const Frame& frame, const cv::Mat& mask, const cv::RotatedRect& boundRect, cv::Point2f grndPoint);
	void confirm(std::shared_ptr<CredResult> result);
	void init(const Frame& frame, const cv::Mat& mask, const cv::RotatedRect& boundRect, cv::Point2f grndPoint);

private:
	cv::MatND* pastHistogram;

	cv::MatND* calcHistogram(const std::shared_ptr<cv::Mat>& frame, const cv::RotatedRect& boundRect, const cv::Mat& mask);
};

#endif /* SRC_HISTOGRAMID_H_ */
