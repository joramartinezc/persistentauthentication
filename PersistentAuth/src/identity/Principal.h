/*******************************************************************************
 * Copyright 2017 Jorge Andres Martinez Castillo
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License.  You may obtain a copy
 * of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations under
 * the License.
 ******************************************************************************/
/*
 * Principal.h
 *
 *  Created on: Apr 5, 2017
 *      Author: jorge
 */

#ifndef SRC_PRINCIPAL_H_
#define SRC_PRINCIPAL_H_

#include <string>
#include <vector>
#include <memory>

#include "Credential.h"
#include "kalmanTracking/KalmanTracker.h"

class Principal {
public:
	Principal();
	virtual ~Principal();
	const std::shared_ptr<std::vector<std::shared_ptr<Credential>>> getCredentials() const;
	void setCredentials(std::shared_ptr<std::vector<std::shared_ptr<Credential>>> credentials);
	const std::string& getPrincipalId() const;
	void setPrincipalId(std::string principalId);

private:
	std::string principalId;
	std::shared_ptr<std::vector<std::shared_ptr<Credential>>> credentials;
};

#endif /* SRC_PRINCIPAL_H_ */
