/*******************************************************************************
 * Copyright 2017 Jorge Andres Martinez Castillo
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License.  You may obtain a copy
 * of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations under
 * the License.
 ******************************************************************************/
/*
 * KalmanTracking.h
 *
 *  Created on: Apr 5, 2017
 *      Author: jorge
 */
#include <opencv2/core.hpp>     // Basic OpenCV structures (cv::Mat, Scalar, Rect)
#include <opencv2/video/tracking.hpp>

#ifndef SRC_KALMANID_H_
#define SRC_KALMANID_H_

using namespace cv;

#include "Credential.h"
#include "kalmanTracking/KalmanTracker.h"

class KalmanId: public Credential {
public:
	KalmanId();
	virtual ~KalmanId();

	void init(const Frame& frame, cv::Point2f grndPoint, std::shared_ptr<KalmanTracker> tracker);
	std::shared_ptr<CredResult> checkSimilarity(const Frame& frame, const cv::Mat& mask, const cv::RotatedRect& boundRect, cv::Point2f grndPoint);
	void confirm(std::shared_ptr<CredResult> result);

private:
	std::shared_ptr<KalmanTracker> tracker;
};

#endif /* SRC_KALMANID_H_ */
