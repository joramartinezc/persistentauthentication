/*******************************************************************************
 * Copyright 2017 Jorge Andres Martinez Castillo
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License.  You may obtain a copy
 * of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations under
 * the License.
 ******************************************************************************/
/*
 * Credential.h
 *
 *  Created on: Apr 5, 2017
 *      Author: jorge
 */

#ifndef SRC_CREDENTIAL_H_
#define SRC_CREDENTIAL_H_

#include <memory>
#include <opencv2/core.hpp>     // Basic OpenCV structures (cv::Mat, Scalar)
#include "CredResult.h"
#include "../entities/Frame.h"

class Credential {
public:
	virtual std::shared_ptr<CredResult> checkSimilarity(const Frame& frame, const cv::Mat& mask, const cv::RotatedRect& boundRect, cv::Point2f grndPoint)  = 0;
	virtual void confirm(std::shared_ptr<CredResult> result)  = 0;
	virtual ~Credential(){}
};

#endif /* SRC_CREDENTIAL_H_ */
