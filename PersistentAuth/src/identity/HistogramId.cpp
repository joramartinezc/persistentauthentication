////////////////////////////////////////////////////////////////////////////////
// Copyright 2017 Jorge Andres Martinez Castillo
// 
// Licensed under the Apache License, Version 2.0 (the "License"); you may not
// use this file except in compliance with the License.  You may obtain a copy
// of the License at
// 
//   http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
// WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
// License for the specific language governing permissions and limitations under
// the License.
////////////////////////////////////////////////////////////////////////////////
/*
 * HistogramId.cpp
 *
 *  Created on: Apr 5, 2017
 *      Author: jorge
 */

#include "HistogramId.h"

#include <core/base.hpp>
#include <core/fast_math.hpp>
#include <core/hal/interface.h>
#include <core/mat.hpp>
#include <core/mat.inl.hpp>
#include <core/types.hpp>
#include <imgproc/imgproc_c.h>
#include <imgproc/types_c.h>
#include <imgproc.hpp>
#include <algorithm>
#include <iomanip>
#include <iostream>
#include <memory>
#include <string>

//debug
#define SRC_HISTOGRAMID_H_DEBUG_disabled

#ifdef SRC_HISTOGRAMID_H_DEBUG
#include <opencv2/plot.hpp>
#include <sstream>  // string to number conversion
#include "../utils/DebugWin.h"
#include "../utils/Utils.h"
#include <thread>
#endif /* SRC_HISTOGRAMID_H_DEBUG */

using namespace cv;
using namespace std;

HistogramId::HistogramId() {
	pastHistogram = NULL;
}

HistogramId::~HistogramId() {
}

void HistogramId::init(const Frame& frame, const cv::Mat& mask, const cv::RotatedRect& boundRect, cv::Point2f grndPoint) {
	MatND* hist_base = calcHistogram(frame.getFrameMat(), boundRect, mask);

	if (pastHistogram == NULL) {
		pastHistogram = hist_base;
	}
}

MatND* HistogramId::calcHistogram(const std::shared_ptr<cv::Mat>& frame, const cv::RotatedRect& boundRect, const cv::Mat& mask) {
	MatND* hist_base = new MatND();
	Mat color_base;
	// get angle and size from the bounding box
	float angle = boundRect.angle;
	Size rectSize = boundRect.size;

	if (boundRect.angle < -45.) {
		angle += 90.0;
		swap(rectSize.width, rectSize.height);
	}
	// get the rotation matrix
	Mat R = getRotationMatrix2D(boundRect.center, angle, 1.0);
	// perform the affine transformation
	Mat rotated, cropped, cropMask;

	//rotate and crop according to bounding box
	cv::warpAffine(*frame.get(), rotated, R, frame->size(), cv::INTER_CUBIC);
	cv::getRectSubPix(rotated, rectSize, boundRect.center, cropped);

	cv::warpAffine(mask, rotated, R, mask.size(), cv::INTER_CUBIC);
	cv::getRectSubPix(rotated, rectSize, boundRect.center, cropMask);

//	cv::Mat cropped = frame(boundRect.boundingRect());
//	cv::Mat cropMask = mask(boundRect.boundingRect());

//	cv::imshow("crop",cropped);

#ifdef SRC_HISTOGRAMID_H_DEBUG
	std::ostringstream winName;
	if (pastHistogram == NULL) {
		winName << "ORIG ";
	}
	winName << "crop: ";
	winName << std::this_thread::get_id();
	Mat resM;
	cropped.copyTo(resM, cropMask);
//    cv::add(resM, Scalar(100, 100, 0), resM, cropMask);
	DebugWin::getInstance().enqueDebug(winName.str(), resM);
#endif /* SRC_HISTOGRAMID_H_DEBUG */

//	cvtColor(cropped, color_base, COLOR_BGR2HSV);
	cvtColor(cropped, color_base, COLOR_BGR2YCrCb);
	/// Using 50 bins for hue and 60 for saturation
	int h_bins = 12;
	int s_bins = 12;
	int histSize[] = { h_bins, s_bins };
//	int histSize[] = { s_bins, s_bins };
	// hue varies from 0 to 179, saturation from 0 to 255
	float h_ranges[] = { 0, 180 };
	float s_ranges[] = { 0, 256 };
//	const float* ranges[] = { h_ranges, s_ranges };
	const float* ranges[] = { s_ranges, s_ranges };
//	int channels[] = { 0, 1 };
	int channels[] = { 1, 2 };
	calcHist(&color_base, 1, channels, cropMask, *hist_base, 2, histSize, ranges, true, false);
//	Utils::printMat(((Mat)*hist_base));
	normalize(*hist_base, *hist_base, 0, 1, NORM_MINMAX, -1, Mat());

#ifdef SRC_HISTOGRAMID_H_DEBUG
	double maxVal = 0;
    int scale = 10;
	minMaxLoc(*hist_base, 0, &maxVal, 0, 0);
    Mat histImage = Mat::zeros(s_bins*scale, h_bins*10, CV_8UC3);
	for (int h = 0; h < h_bins; h++) {
//		cout << "[";
		for (int s = 0; s < s_bins; s++) {
			float binVal = hist_base->at<float>(h, s);
//			cout << setprecision(1) << binVal;
			if (s != s_bins - 1) {
//				cout << ", ";
			} else {
//				cout << "]" << endl;
			}
			int intensity = cvRound(binVal * 255 / maxVal);
			int cr = 255/h_bins*h;
			int cb = 255/s_bins*s;
			int r = intensity + 1.403*((float)cr-128.);
			int g = intensity - 0.714*((float)cr-128.)-0.344*((float)cb-128.);
			int b = intensity + 1.733*((float)cb-128.);
//			cout <<"I "<< intensity << " " <<Scalar(b,g,r) <<endl<<flush;
			Scalar colorH = Scalar(b,g,r);
//			colorH = Scalar::all(intensity);
			cv::rectangle(histImage, Point(s * scale, h * scale), Point((s + 1) * scale - 1, (h + 1) * scale - 1), colorH, CV_FILLED);
		}
	}//cout << endl<<flush;

	std::ostringstream win;
	if (pastHistogram == NULL) {
		win << "ORIG ";
	}
	win << "Hist: ";
	win << std::this_thread::get_id();
	DebugWin::getInstance().enqueDebug(win.str(), histImage);

#endif /* SRC_HISTOGRAMID_H_DEBUG */
	return hist_base;
}

std::shared_ptr<CredResult> HistogramId::checkSimilarity(const Frame& frame, const cv::Mat& mask, const cv::RotatedRect& boundRect, cv::Point2f grndPoint) {

	MatND* hist_base = calcHistogram(frame.getFrameMat(), boundRect, mask);
	double base_base = compareHist(*pastHistogram, *hist_base, CV_COMP_BHATTACHARYYA);

	base_base = base_base*1;

	std::shared_ptr<CredResult> res = std::make_shared<CredResult>(base_base);
	return res;
}

void HistogramId::confirm(std::shared_ptr<CredResult> result) {
}
