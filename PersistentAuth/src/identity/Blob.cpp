////////////////////////////////////////////////////////////////////////////////
// Copyright 2017 Jorge Andres Martinez Castillo
// 
// Licensed under the Apache License, Version 2.0 (the "License"); you may not
// use this file except in compliance with the License.  You may obtain a copy
// of the License at
// 
//   http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
// WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
// License for the specific language governing permissions and limitations under
// the License.
////////////////////////////////////////////////////////////////////////////////
/*
 * Blob.cpp
 *
 *  Created on: Apr 5, 2017
 *      Author: jorge
 */

#include "Blob.h"

Blob::~Blob() {
	// TODO Auto-generated destructor stub
}

Blob::Blob(vector<std::shared_ptr<Principal>> principals, RotatedRect boundingBox, Point2f position) {
	this->principals = principals;
	this->boundingBox = boundingBox;
	this->position = position;
}

int Blob::getNumPeople() {
	return principals.size();
}

RotatedRect Blob::getBoundingBox() {
	return boundingBox;
}

string Blob::getLabel() {
	String label;
	for (unsigned i = 0; i < this->principals.size(); i++) {
		label = label + "." + this->principals[i]->getPrincipalId();
	}
	return label;
}
const Point2f& Blob::getPosition() const {
	return position;
}

vector<std::shared_ptr<Principal>>& Blob::getPrincipals() {
	return principals;
}
