////////////////////////////////////////////////////////////////////////////////
// Copyright 2017 Jorge Andres Martinez Castillo
// 
// Licensed under the Apache License, Version 2.0 (the "License"); you may not
// use this file except in compliance with the License.  You may obtain a copy
// of the License at
// 
//   http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
// WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
// License for the specific language governing permissions and limitations under
// the License.
////////////////////////////////////////////////////////////////////////////////
/*
 * KalmanTracking.cpp
 *
 *  Created on: Apr 5, 2017
 *      Author: jorge
 */

#include "KalmanId.h"

#include "../utils/Utils.h"
#include "kalmanTracking/KalmanResult.h"
#include "kalmanTracking/KalmanTracker.h"

#include <memory>
#include <iostream>
#include <string>
#include <opencv2/imgproc.hpp>
#include <opencv2/highgui.hpp>

KalmanId::KalmanId() {}

KalmanId::~KalmanId() {}

void KalmanId::init(const Frame& frame, cv::Point2f grndPoint, std::shared_ptr<KalmanTracker> tracker) {
	this->tracker = tracker;
	this->tracker->init(frame.getMsec(),grndPoint);
}

std::shared_ptr<CredResult> KalmanId::checkSimilarity(const Frame& frame, const cv::Mat& mask, const cv::RotatedRect& boundRect, cv::Point2f grndPoint) {
	std::shared_ptr<KalmanResult> res;
	res = this->tracker->checkSimilarity(frame.getMsec(),grndPoint);
	return res;
}

void KalmanId::confirm(std::shared_ptr<CredResult> result){
	std::shared_ptr<KalmanResult> kres = std::static_pointer_cast<KalmanResult>(result);
	tracker->confirm(kres);
}
