/*******************************************************************************
 * Copyright 2017 Jorge Andres Martinez Castillo
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License.  You may obtain a copy
 * of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations under
 * the License.
 ******************************************************************************/
/*
 * Environment.h
 *
 *  Created on: Apr 16, 2017
 *      Author: jorge
 */

#ifndef SCENEMODEL_ENVIRONMENT_H_
#define SCENEMODEL_ENVIRONMENT_H_
#include <memory>
#include <vector>
#include <map>

#include "../identity/Principal.h"
#include "Camera.h"
#include "AuthorizationZone.h"
#include "AuthenticationZone.h"
#include "environmentSettings/CamSettings.h"
#include "environmentSettings/ZoneSettings.h"

class Environment {
public:
	static Environment& getInstance();
	virtual ~Environment();

	void notifyZonesOfCamera(unsigned idCamera, std::shared_ptr<Principal> principal, cv::Point position);
	void notifyCamerasOfZone(unsigned idZone, std::shared_ptr<Principal> principal);
	void registerCameras(std::vector<CamSettings> params);
	void registerAuthenticationZone(std::vector<ZoneSettings> params);
	void associateCameraToAuthenticationZone(int cameraId, int zoneId);
	void associateAuthorizationZoneToCamera(int zoneId, int cameraId);
	std::map<int,std::shared_ptr<cv::Mat>> retrieveResults();
	void stopCameras();
	std::shared_ptr<Zone> getZone(unsigned idZone);
	std::shared_ptr<Camera> getCamera(unsigned idCam);
	std::map<int, std::shared_ptr<Camera>> cameras;// TODO make cameras private
	const std::map<std::string, std::shared_ptr<Principal> >& principalReport() const;
private:
	Environment();
	std::map<int, std::vector<std::shared_ptr<Camera>>> camerasOfZones;
	std::map<int, std::vector<std::shared_ptr<AuthorizationZone>>> zonesOfCamera;

	std::map<int, std::shared_ptr<Zone>> zones;
};

#endif /* SCENEMODEL_ENVIRONMENT_H_ */
