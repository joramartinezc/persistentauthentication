////////////////////////////////////////////////////////////////////////////////
// Copyright 2017 Jorge Andres Martinez Castillo
// 
// Licensed under the Apache License, Version 2.0 (the "License"); you may not
// use this file except in compliance with the License.  You may obtain a copy
// of the License at
// 
//   http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
// WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
// License for the specific language governing permissions and limitations under
// the License.
////////////////////////////////////////////////////////////////////////////////
/*
 * Environment.cpp
 *
 *  Created on: Apr 16, 2017
 *      Author: jorge
 */

#include <iostream>

#include "Environment.h"
#include "../utils/Utils.h"
#include "../identity/IdentityAnalyser.h"

Environment& Environment::getInstance(){
  static Environment instance;
  return instance;
}

Environment::Environment() {}

Environment::~Environment() {
	// TODO Auto-generated destructor stub
}

const std::map<std::string, std::shared_ptr<Principal> >& Environment::principalReport() const {
	return IdentityAnalyser::getPpals();
}

void Environment::notifyCamerasOfZone(unsigned idZone, std::shared_ptr<Principal> principal) {
	Utils::log(std::vector<std::string>({"notifyCamerasOfZone: ", Utils::n2s(idZone,1), " ", principal->getPrincipalId()}));
	std::vector<std::shared_ptr<Camera>> cameras;
	cameras = camerasOfZones[idZone];
	for (unsigned i = 0; i < cameras.size(); ++i) {
		std::shared_ptr<Camera> camera = cameras[i];
		camera->registerPrincipal(idZone,principal);
	}
}

//TODO call from Camera
void Environment::notifyZonesOfCamera(unsigned idCamera, std::shared_ptr<Principal> principal, Point position) {
	std::vector<std::shared_ptr<AuthorizationZone>> zones;
	zones = zonesOfCamera[idCamera];
	for (unsigned i = 0; i < zones.size(); ++i) {
		std::shared_ptr<AuthorizationZone> zone = zones[i];
		if(zone->containsPoint(position)) {
			zone->authorizate(principal);
		}
	}
}

void Environment::registerCameras(std::vector<CamSettings> params) {
	for (std::vector<CamSettings>::iterator it = params.begin(); it != params.end(); ++it) {
		std::shared_ptr<Camera> cam = std::make_shared<Camera>((*it).idCamera, (*it).sourcePath,(*it).settingsFilePath, (*it).homographyFilePath, (*it).orientation, (*it).isFisheye, (*it).focalLenght);
		cam->setDelay((*it).delay);
		cam->initWatch();
		cameras[(*it).idCamera] = cam;
	}
}

void Environment::registerAuthenticationZone(std::vector<ZoneSettings> params) {
	for (std::vector<ZoneSettings>::iterator it = params.begin(); it != params.end(); ++it) {

		std::vector<std::vector<cv::Point> > contours;
		cv::FileStorage fs;
		fs.open((*it).contoursFilePath, cv::FileStorage::READ);
		fs [ "contours" ] >> contours;
		fs.release();

		std::shared_ptr<AuthenticationZone> zone = std::make_shared<AuthenticationZone>((*it).idZone,(*it).name,contours[0]);
		zones[(*it).idZone] = zone;
	}
}

void Environment::associateCameraToAuthenticationZone(int cameraId, int zoneId) {
	camerasOfZones[zoneId].push_back(cameras[cameraId]);
}

void Environment::associateAuthorizationZoneToCamera(int zoneId, int cameraId) {
	std::shared_ptr<Zone> zone = zones[zoneId];
	std::shared_ptr<AuthorizationZone> aZone;
	aZone = std::dynamic_pointer_cast<AuthorizationZone>(zone);

	zonesOfCamera[cameraId].push_back( aZone );
}

std::map<int,std::shared_ptr<cv::Mat>> Environment::retrieveResults() {
	std::map<int,std::shared_ptr<cv::Mat>> results;
	for (auto& kv : cameras) {
		std::shared_ptr<cv::Mat> res = kv.second->retrieveResult();
		if(res == nullptr){
			kv.second->stop();
			cameras.erase(kv.first);
		} else {
			results[kv.first] = res;
		}
	}
	return results;
}

void Environment::stopCameras() {
	for(auto cam : cameras) {
		cam.second->stop();
	}
}

std::shared_ptr<Zone> Environment::getZone(unsigned idZone) {
	return zones[idZone];
}

std::shared_ptr<Camera> Environment::getCamera(unsigned idCam) {
	return cameras[idCam];
}
