////////////////////////////////////////////////////////////////////////////////
// Copyright 2017 Jorge Andres Martinez Castillo
// 
// Licensed under the Apache License, Version 2.0 (the "License"); you may not
// use this file except in compliance with the License.  You may obtain a copy
// of the License at
// 
//   http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
// WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
// License for the specific language governing permissions and limitations under
// the License.
////////////////////////////////////////////////////////////////////////////////
/*
 * Zone.cpp
 *
 *  Created on: Apr 16, 2017
 *      Author: jorge
 */

#include <iostream>
#include <opencv2/imgproc.hpp>

#include "Zone.h"

Zone::Zone(int id, const std::string& name, const std::vector<cv::Point>& contour) {
	this->id=id;
	this->name = std::string(name);

	this->contour = std::vector<cv::Point>(contour.size());
	for( int i = 0; i < contour.size();i++) {
		this->contour[i] = cv::Point(contour[i].x,contour[i].y);
	}
}

Zone::~Zone() {
}

bool Zone::containsPoint(cv::Point point) {
	const std::vector<cv::Point> cont = this->contour;
	double res = cv::pointPolygonTest(cont, point, false);

	if (res >= 0) {
		return true;
	}else{
		return false;
	}
}

std::vector<cv::Point> Zone::getContour() {
	return contour;
}
