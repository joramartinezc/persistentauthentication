/*******************************************************************************
 * Copyright 2017 Jorge Andres Martinez Castillo
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License.  You may obtain a copy
 * of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations under
 * the License.
 ******************************************************************************/
/*
 * Zone.h
 *
 *  Created on: Apr 16, 2017
 *      Author: jorge
 */

#include <vector>
#include <string>
#include <opencv2/core.hpp>

#ifndef SCENEMODEL_ZONE_H_
#define SCENEMODEL_ZONE_H_

class Zone {
public:
	Zone(int id, const std::string& name, const std::vector<cv::Point>& contour);
	virtual ~Zone();
	bool containsPoint(cv::Point point);
	std::vector<cv::Point> getContour();

	const std::string& getId() const {
		return id;
	}

	const std::string& getName() const {
		return name;
	}

private:
	std::vector<cv::Point> contour;
	std::string name;
	std::string id;
};

#endif /* SCENEMODEL_ZONE_H_ */
