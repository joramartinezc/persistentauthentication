////////////////////////////////////////////////////////////////////////////////
// Copyright 2017 Jorge Andres Martinez Castillo
// 
// Licensed under the Apache License, Version 2.0 (the "License"); you may not
// use this file except in compliance with the License.  You may obtain a copy
// of the License at
// 
//   http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
// WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
// License for the specific language governing permissions and limitations under
// the License.
////////////////////////////////////////////////////////////////////////////////
/*
 * Camera.cpp
 *
 *  Created on: Apr 15, 2017
 *      Author: jorge
 */


#include <memory>
#include <vector>
#include <algorithm>
#include <iostream>
#include <thread> // sleep
#include <math.h>

#include <opencv2/core.hpp>
#include <opencv2/core/persistence.hpp>
#include <opencv2/imgproc.hpp>
#include <opencv2/highgui.hpp>  // OpenCV window I/O
#include <opencv2/imgproc.hpp>
#include <opencv2/calib3d.hpp>

#include "../utils/Utils.h"
#include "../utils/LimitBuffer.h"
#include "../identity/Credential.h"
#include "../identity/Blob.h"
#include "../identity/HistogramId.h"
#include "../entities/Frame.h"
#include "Environment.h"
#include "Camera.h"
#include "../identity/IdentityAnalyser.h"

using namespace std;

Camera::Camera(int idCam, const string& sourcePath, const string& settingsFilePath, const string& homographyFilePath, Camera::CamOrientation orientation, bool isFisheye, double focalLenght) {
	this->id = idCam;
	FileStorage fs;
	fs.open(homographyFilePath, FileStorage::READ);
	Mat H;
	fs["homography"] >> H;
	fs.release();

	fs.open(settingsFilePath, FileStorage::READ);
	int w, h;
	fs["image_width"] >> w;
	fs["image_height"] >> h;
	fs.release();

	this->orientation = orientation;
	this->H = H;
	this->resolution = Size2i(w, h);

	stringstream conv;
	if ((sourcePath.find_first_not_of("0123456789") == string::npos)) {
		conv << sourcePath;
		int camId;
		conv >> camId;
		video.open(camId);
	} else {
		video.open(sourcePath);
	}
	if (!video.isOpened()) {
		cerr << "Could not open reference " << sourcePath << endl;
	}

	this->cameraTx = make_shared<CameraTransformation>(settingsFilePath, focalLenght, 800, 550, isFisheye);
//	this->bgnSub = BgnSubtraction();
//	this->treshold = Treshold();
//	this->denoise = Denoise();
//	this->dilate = Dilate();

//	this->idAnalyser = make_shared<IdentityAnalyser>();
	fps = video.get(CV_CAP_PROP_FPS);
//	fps = 2;
	std::cout << "cam[ "<< this->id << " ] : fps="<<fps<<std::endl<<std::flush;
	Utils::log(vector<string>({"cam[",Utils::n2s(this->id),"] : fps=",Utils::n2s(fps)}));
}

Point2f Camera::getGroundPoint(RotatedRect boundingRect) {
	Mat mask1 = cv::Mat(resolution, CV_8UC1, cv::Scalar(0)); // creates an empty mask of the image size
	Mat mask2 = cv::Mat(resolution, CV_8UC1, cv::Scalar(0));

	Utils::rotatedRectangle(mask1, boundingRect, Scalar(255), 1, LINE_8, 0, true);

	vector<Vec4i> hierarchy;
	vector<vector<Point> > contours;
	cv::findContours(mask1, contours, hierarchy, RETR_EXTERNAL, CV_CHAIN_APPROX_TC89_KCOS, Point(0, 0));

	Point2i center = Point2i(resolution.width / 2, resolution.height / 2);
	double dist = -pointPolygonTest(contours[0], center, true);
	dist = std::max(1.0, dist);
	cv::circle(mask2, center, dist, Scalar(255), 2);
	Mat mask = mask1.mul(mask2);
//	cv::dilate(mask, mask, getStructuringElement(MORPH_ELLIPSE, Size(10, 10)));
//	imshow("RECT", mask);
	Mat locations;
	cv::findNonZero(mask, locations);

	if (locations.rows > 0) {
		// access pixel coordinates
		Point groundPoint = locations.at<Point>(0);
		return groundPoint;
	} else {
		cerr << "could not find ground point: (" << boundingRect.center.x << "," << boundingRect.center.y << "):" << boundingRect.size.height << "," << boundingRect.size.width << endl;
		return Point(0, 0);
	}
}

void Camera::stop() {
	this->RUNNING = false;
	threadCapture->join();
//	threadWatch->join();
}
void Camera::record() {

	try {
		while (this->RUNNING) {
			video.grab();

			//TODO remove auth test
			double msec = video.get(CV_CAP_PROP_POS_MSEC);
			if(cvRound(msec) == cvRound(19400) && id == 206 && false) {
				std::shared_ptr<Principal> aga = std::make_shared<Principal>();
				aga->setPrincipalId("Agnieszka");
				Environment::getInstance().notifyCamerasOfZone(1, aga);
		//			system("paplay /usr/share/sounds/freedesktop/stereo/complete.oga");
			}
			if(cvRound(msec) == cvRound(6600) && id == 206 && false) {
				std::shared_ptr<Principal> jorge = std::make_shared<Principal>();
				jorge->setPrincipalId("Jorge");
				Environment::getInstance().notifyCamerasOfZone(1, jorge);
		//			system("paplay /usr/share/sounds/freedesktop/stereo/complete.oga");
			}
			if(fps == 0) {//TODO delete
				std::this_thread::sleep_for(std::chrono::hours(1));
			}
			std::this_thread::sleep_for(std::chrono::milliseconds(1000 / fps));
		//		std::this_thread::sleep_for(std::chrono::milliseconds(1000 / 2));
		}
	} catch (Exception& e) {
		Utils::log(vector<string>({"ERROR ",e.err,": ",e.msg}));
	}
}

int Camera::getId() const {
	return id;
}

std::shared_ptr<cv::Mat> Camera::watchFrame() {
	std::shared_ptr<cv::Mat> result = std::make_shared<cv::Mat>();

	if(principalToAuth != nullptr){
		Utils::log(vector<std::string>({"auth frame: ", Utils::n2s(video.get(CV_CAP_PROP_POS_MSEC),5)}));
	} else {
//		Utils::log(vector<std::string>({"watch frame: ", Utils::n2s(video.get(CV_CAP_PROP_POS_FRAMES),5)}));
	}
	std::lock_guard<std::mutex> lock(Camera::frameMutex);
	if(principalToAuth != nullptr) {
//		Utils::log(vector<std::string>({"get Lock"}));
	}

	//	pMOG->setShadowThreshold(0.2);

	//	HOGDescriptor hog;
	//hog.load("src/flow/hog/cvHOGClassifier.yaml");
	//	hog.setSVMDetector(HOGDescriptor::getDefaultPeopleDetector());
	cv::Mat frame;
	// Get the frame
	double msec = video.get(CV_CAP_PROP_POS_MSEC);
	video.retrieve(frame);
	if (frame.empty()) {
//		cout << "cam[ " << this->id << " ] : " << " < < <  Video over!  > > > ";
		return nullptr;
	}
	//		cv::cvtColor(frame, frame, cv::COLOR_RGB2HSV);

	frame = cameraTx->apply(frame);
	changeResolution(800,600,frame);
//	Mat frameN = histEqu.apply(frame);
	Mat mask = bgnSub.apply(frame);
	Mat maskFilt = treshold.apply(mask);
	Mat maskDenoise = denoise.apply(maskFilt);
	Mat maskDilate = dilate.apply(maskDenoise);
//	Mat maskDilate = (maskDenoise);


	//TODO if there are blobs continue
	double min = 1, max = 0;
	//		cv::minMaxLoc(maskDenoise, &min, &max);
	if (min != max) {
		// contour detection
		//Canny(maskDil, topologic, LOC_THRESH1, LOC_THRESH2);

		//Topological analysis with border following
		vector<vector<Point> > contours;
		vector<Vec4i> hierarchy;
		cv::findContours(maskDilate, contours, hierarchy, RETR_EXTERNAL, CV_CHAIN_APPROX_TC89_KCOS, Point(0, 0));

		// Draw contours
		std::vector<RotatedRect> boundRect(contours.size());
		for (unsigned i = 0; i < contours.size(); i++) {
			boundRect[i] = cv::minAreaRect(Mat(contours[i]));
		}

		//vector<Rect> boundRect;
		//hog.detectMultiScale(frame, boundRect, 0, Size(8,8), Size(32,32), 1.05, 2);
		std::shared_ptr<cv::Mat> matPtr = std::make_shared<cv::Mat>(frame);//TODO try to avoid this Mat copy
		Frame frameObj = Frame(matPtr,msec);
		std::vector<shared_ptr<Blob>> blobs;
		if (idZoneToAuth >= 0 && principalToAuth != nullptr) {
			blobs = IdentityAnalyser::createBlobs(*this, frameObj, maskDilate, boundRect, idZoneToAuth, principalToAuth);
		} else {
			blobs = IdentityAnalyser::createBlobs(*this, frameObj, maskDilate, boundRect, -1, nullptr);
		}

//		Mat frameRGB;
//		cv::cvtColor(mask, frameRGB, cv::COLOR_GRAY2RGB);
        frame.copyTo(*result.get());
        cv::add(*result.get(), Scalar(100, 100, 0), *result.get(), maskDilate);
//		cout << "cam[ " << this->id << " ] : " << "blobs: " << blobs.size() << endl << endl << flush;

        std::vector<shared_ptr<Blob>>::iterator it;
		for (it = blobs.begin(); it != blobs.end(); ++it) {
			shared_ptr<Blob> blob = (*it);
			RotatedRect box = blob->getBoundingBox();
			string label = blob->getLabel();

			Scalar color = Scalar(0, 0, 255);
			Utils::rotatedRectangle(*result.get(), box, color, 2, 8, 0);

			putText(*result.get(), "X", blob->getPosition(), CV_FONT_HERSHEY_SIMPLEX, 1, cv::Scalar(0, 0, 255));
			putText(*result.get(), label, box.center, CV_FONT_HERSHEY_SIMPLEX, 1, cv::Scalar(0, 255, 255));
		}

		buffer.enqueue(result);
	}

	if(idZoneToAuth >= 0 && principalToAuth != nullptr) {
		idZoneToAuth = -1;
		principalToAuth = nullptr;
//		Utils::log(vector<std::string>({"watchFrame end: "}));
	}
	return result;
}

void Camera::watch() {

//	this->threadCapture = make_shared<thread>(&Camera::record, this);
	thread t(&Camera::record, this);

	try {
		std::this_thread::sleep_for(std::chrono::milliseconds(500));
		while (this->RUNNING) {
			float iniT = chrono::duration_cast<chrono::milliseconds>(chrono::steady_clock::now().time_since_epoch()).count();
			std::shared_ptr<cv::Mat> res = this->watchFrame();
			if (res == nullptr) {
				break;
			}

			float endT = chrono::duration_cast<chrono::milliseconds>(chrono::steady_clock::now().time_since_epoch()).count();
			float tDif = (endT - iniT);
			float actualFps = 1. / tDif * 1000.;

			if(fps == 0) {//TODO delete
				std::this_thread::sleep_for(std::chrono::hours(1));
			}
			if (actualFps > fps) {
				int sleepTime = 1000/fps - tDif;
				std::this_thread::sleep_for(std::chrono::milliseconds(sleepTime));
//				Utils::log(vector<std::string>({"cam[", Utils::n2s(this->id), "] : ", Utils::n2s((actualFps))," possible fps (of ",Utils::n2s(fps)," fps)"}));
			}
			cv::putText(*res.get(),Utils::n2s(((int)actualFps))+"/"+Utils::n2s(fps),Point(0,15), FONT_HERSHEY_DUPLEX, 0.5, cv::Scalar(0, 255, 0));
		}
		this->RUNNING = false;
	} catch (Exception e) {
		Utils::log(vector<string>({"ERROR ",": ",e.msg}));
	}
	t.join();
}

void Camera::initWatch() {
	video.set(CV_CAP_PROP_POS_MSEC,this->delay);
	threadCapture = make_shared<thread>(&Camera::watch, this);
}

void Camera::registerPrincipal(unsigned idZone, std::shared_ptr<Principal> principal) {
//	watchFrame(idZone, principal);
	idZoneToAuth = idZone;
	principalToAuth = principal;
}

std::shared_ptr<cv::Mat> Camera::retrieveResult() {
	if (!RUNNING) {
		return nullptr;
	}
	while (buffer.size() <= 0) {
		std::this_thread::sleep_for(std::chrono::milliseconds(50));
	}
	return buffer.dequeue();
}

std::vector<Point2f> Camera::frameToReal(std::vector<Point2f> points) {
	std::vector<Point2f> real;
	cv::perspectiveTransform(points, real, H.inv());
	return real;
}

std::vector<Point2f> Camera::realToFrame(std::vector<Point2f> points) {
	std::vector<Point2f> frame;
	cv::perspectiveTransform(points, frame, H);
	return frame;
}

cv::Mat Camera::changeResolution(int width, int height, cv::Mat& frame) {
	if(width != resolution.width || resolution.height != height) {
		Point2f newRes[4];
		newRes[0] = Point2f(0,0);
		newRes[1] = Point2f(0,height);
		newRes[2] = Point2f(width,height);
		newRes[3] = Point2f(width,0);
		Point2f oldRes[4];
		oldRes[0] = Point2f(0,0);
		oldRes[1] = Point2f(0,resolution.height);
		oldRes[2] = Point2f(resolution.width,resolution.height);
		oldRes[3] = Point2f(resolution.width,0);

		cv::Mat resizeT = cv::getPerspectiveTransform(oldRes, newRes);
		H = resizeT*H;

		resolution.width = width;
		resolution.height = height;
	}

	cv::resize(frame, frame, cv::Size(width, height));
	return frame;
}

template class LimitBuffer<std::shared_ptr<cv::Mat>> ;
