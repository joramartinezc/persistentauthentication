/*******************************************************************************
 * Copyright 2017 Jorge Andres Martinez Castillo
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License.  You may obtain a copy
 * of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations under
 * the License.
 ******************************************************************************/
/*
 * CamSettings.h
 *
 *  Created on: Apr 17, 2017
 *      Author: jorge
 */

#ifndef SCENEMODEL_ENVIRONMENTSETTINGS_CAMSETTINGS_H_
#define SCENEMODEL_ENVIRONMENTSETTINGS_CAMSETTINGS_H_

#include <string>
#include "../Camera.h"

class CamSettings {
public:
	CamSettings();
	virtual ~CamSettings();

	int idCamera;
	float delay;
	std::string sourcePath;
	std::string settingsFilePath;
	std::string homographyFilePath;
	Camera::CamOrientation orientation;
	bool isFisheye;
	double focalLenght;
};

#endif /* SCENEMODEL_ENVIRONMENTSETTINGS_CAMSETTINGS_H_ */
