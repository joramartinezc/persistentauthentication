/*******************************************************************************
 * Copyright 2017 Jorge Andres Martinez Castillo
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License.  You may obtain a copy
 * of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations under
 * the License.
 ******************************************************************************/
/*
 * Camera.h
 *
 *  Created on: Apr 15, 2017
 *      Author: jorge
 */
//
#ifndef SCENEMODEL_CAMERA_H_
#define SCENEMODEL_CAMERA_H_

#include <memory>
#include <thread> // sleep

#include "../flow/HistogramEqualization.h"
#include "../flow/BgnSubtraction.h"
#include "../flow/CameraTransformation.h"
#include "../flow/Denoise.h"
#include "../flow/Dilate.h"
#include "../flow/Transformation.h"
#include "../flow/Treshold.h"

#include "../identity/Principal.h"
#include "../utils/SyncVideoCapture.h"

//class IdentityAnalyser;
#include "../utils/LimitBuffer.h"
//#include "../identity/IdentityAnalyser.h"

#include <opencv2/core.hpp>

class Camera {
public:
	enum CamOrientation { TOP/*, SIDE*/ };
	Camera(int idCam, const std::string& sourcePath, const std::string& settingsFilePath, const std::string& homographyFilePath, Camera::CamOrientation orientation, bool isFisheye, double focalLenght);
//
	bool RUNNING = true;//TODO make private

	cv::Point2f getGroundPoint(cv::RotatedRect boundingRect);

	void initWatch();
	void registerPrincipal(unsigned idZone, std::shared_ptr<Principal> principal);
	void stop();
	std::shared_ptr<cv::Mat> retrieveResult();

	std::vector<cv::Point2f> frameToReal(std::vector<cv::Point2f> points);
	std::vector<cv::Point2f> realToFrame(std::vector<cv::Point2f> points);

	const cv::Mat& getH() const {
		return H;
	}

	CamOrientation getOrientation() const {
		return orientation;
	}

	const cv::Size2i& getResolution() const {
		return resolution;
	}

	bool isRunning() const {
		return RUNNING;
	}

	float getDelay() const {
		return delay;
	}

	void setDelay(float delay = 0) {
		this->delay = delay;
	}

	int getId() const;

	int fps;//TODO make fps private
private:
	int id;
	float delay = 0;

	cv::Mat H;//Homography
	CamOrientation orientation;
	cv::Size2i resolution;

	HistogramEqualization histEqu;
	std::shared_ptr<CameraTransformation> cameraTx;
	BgnSubtraction bgnSub;
	Treshold treshold;
	Denoise denoise;
	Dilate dilate;

	SyncVideoCapture video;
	std::shared_ptr<std::thread> threadCapture;
	std::shared_ptr<std::thread> threadWatch;

	LimitBuffer<std::shared_ptr<cv::Mat>> buffer;
//
	int idZoneToAuth = -1;
	std::shared_ptr<Principal> principalToAuth;
//
	std::shared_ptr<cv::Mat> watchFrame();
	void record();
	void watch();
	std::mutex frameMutex;

	cv::Mat changeResolution(int height, int width, cv::Mat& frame);
};

#endif /* SCENEMODEL_CAMERA_H_ */
