/*******************************************************************************
 * Copyright 2017 Jorge Andres Martinez Castillo
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License.  You may obtain a copy
 * of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations under
 * the License.
 ******************************************************************************/
/*
 * AuthorizationZone.h
 *
 *  Created on: Apr 16, 2017
 *      Author: jorge
 */

#ifndef SCENEMODEL_AUTHORIZATIONZONE_H_
#define SCENEMODEL_AUTHORIZATIONZONE_H_

#include <memory>

#include "../identity/Principal.h"
#include "Zone.h"

class AuthorizationZone : public Zone {
public:
	using Zone::Zone;
	virtual ~AuthorizationZone();
	void authorizate(std::shared_ptr<Principal> principal);
};

#endif /* SCENEMODEL_AUTHORIZATIONZONE_H_ */
